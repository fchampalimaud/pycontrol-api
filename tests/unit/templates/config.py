# !/usr/bin/python3
# -*- coding: utf-8 -*-
# pylint: disable=invalid-name

""" templates.config

"""

import os

board_1_serial_port = "/dev/tty.usbmodem1472"

template_base_path = os.path.dirname(os.path.abspath(__file__))
test_install_framework_template_path = os.path.join(template_base_path, os.path.normpath('test_board/test_install_framework'))
