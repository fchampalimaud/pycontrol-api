import logging
from pycontrolapi.models.project import Project
from pycontrolapi.models.experiment import Experiment


def setup_default_logger(logger_name, console_handler=True, file_handler=True):
    """
    Setup default logger with fine detail formatter and console and/or file handlers
    """
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)

    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(name)s | %(module)s | %(funcName)s | %(message)s',
                                  datefmt='%d/%m/%Y %I:%M:%S')

    if file_handler:
        fh = logging.FileHandler(LOG_FILENAME)
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(formatter)
        logger.addHandler(fh)

    if console_handler:
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)
        logger.addHandler(ch)


setup_default_logger('pycontrolapi', console_handler=True, file_handler=False)



proj = Project()

#### CONFIGURE BOARDS ####

board1 = Project.create_board()
board2 = Project.create_board()

board1.name = 'board-1'
board1.serial_port = '/dev/ttyACM0'

proj.add_board(board1)
proj.add_board(board2)

#### CONFIGURE TASKS ####

task1 = Project.create_task()
task2 = Project.create_task()

task1.path = '/home/ricardo/bitbucket/pycontrolgui/examples/ines_vaz_project/tasks/blinker.py'

proj.add_task(task1)
proj.add_task(task2)

#### CONFIGURE EXPERIMENTS ####

exp1 = Project.create_experiment()
exp2 = Project.create_experiment()

proj.add_experiment(exp1)
proj.add_experiment(exp2)

#### CONFIGURE SETUPS ####

setup1 = Experiment.create_setup()

setup1.name 	= 'Setup1'
setup1.board 	= board1
setup1.task 	= task1

exp1.add_setup(setup1)


proj.save('/home/ricardo/bitbucket/pycontrolapi/examples/project1')

board1.install_framework( '/home/ricardo/bitbucket/pycontrolgui/pycontrolgui/resources/default_framework/pyControl' )