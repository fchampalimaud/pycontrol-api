# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrol.api.models.test_state_machine

"""

import os
import shutil
from datetime import datetime
import logging
import time

from unittest import TestCase

from pycontrolapi.models.pycontrolapi import PyControlAPI
from pycontrolapi.models.task import Task
from pycontrolapi.models.project import Project
from pycontrolapi.models.experiment import Experiment
from pycontrolapi.models.subject import Subject
from pycontrolapi.models.setup import Setup
from pycontrolapi.models.board.board import Board
from pycontrolapi.models.session.run_session import RunSession
from pycontrolapi.host.exceptions.pyboard_error import PyBoardError
from serial.serialutil import SerialException

__author__ = "Carlos Mão de Ferro"
__copyright__ = ""
__credits__ = "Carlos Mão de Ferro"
__license__ = "MIT"
__version__ = "0.0"
__maintainer__ = ["Ricardo Ribeiro", "Carlos Mão de Ferro"]
__email__ = ["ricardojvr at gmail.com", "cajomferro at gmail.com"]
__status__ = "Development"

TEST_DIR_PATH = os.path.dirname(os.path.abspath(__file__))


class TestSetupRunInvalidSerialPort(TestCase):
    """
    Testing run setup
    For each test, input folder and project folder (inside input folder) must exist
    For each test, a project output folder is created (inside output folder). In a real case
    scenario, the project input folder is the same for output but here we want to ensure that
    input folder remains untouched.
    """

    def setUp(self):
        self.app = PyControlAPI()

        self._base_output = os.path.join(TEST_DIR_PATH, 'output')
        self._base_input = os.path.join(TEST_DIR_PATH, 'input')

        project_output_folder = os.path.join(self._base_output, "output_setup_run_invalid_serial_port")
        project_template_folder = os.path.join(self._base_input, "input_setup_run_invalid_serial_port")
        if os.path.exists(project_output_folder):
            shutil.rmtree(project_output_folder)
        # ensure that project template folder remains untouched
        shutil.copytree(os.path.abspath(project_template_folder), os.path.abspath(project_output_folder))

        project_name = "proj1"
        project_full_path = os.path.join(project_output_folder, project_name)

        # load project
        self.proj1 = self.app.load_project(project_full_path)

    def test_open_invalid_port(self):
        experiment_name = next((experiment for experiment in self.proj1.experiments if experiment.name == "FOXp1_group_2"), None)
        self.assertIsNotNone(experiment_name)
        setup1 = next((setup for setup in experiment_name.setups if setup.setup_id == "1"), None)
        self.assertIsNotNone(setup1)

        setup1.run()
        self.assertEqual(len(setup1.running_sessions()), 1)
        time.sleep(2)
        self.assertEqual(len(setup1.running_sessions()), 1)
        setup1.update_data()
        self.assertEqual(len(setup1.running_sessions()), 0)

    def tearDown(self):
        self.app.save_project(self.proj1.name)

app_logger = logging.getLogger(__name__)
