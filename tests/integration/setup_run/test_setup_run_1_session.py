# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrol.api.models.test_state_machine

"""

import os
import shutil
import logging
import time

from unittest import TestCase

from pycontrolapi.models.pycontrolapi import PyControlAPI
from pycontrolapi.models.session.run_session import RunSession
from serial.serialutil import SerialException

__author__ = "Carlos Mão de Ferro"
__copyright__ = ""
__credits__ = "Carlos Mão de Ferro"
__license__ = "MIT"
__version__ = "0.0"
__maintainer__ = ["Ricardo Ribeiro", "Carlos Mão de Ferro"]
__email__ = ["ricardojvr at gmail.com", "cajomferro at gmail.com"]
__status__ = "Development"

TEST_DIR_PATH = os.path.dirname(os.path.abspath(__file__))


class TestSetupRun1Session(TestCase):
    """
    Testing run setup
    For each test, input folder and project folder (inside input folder) must exist
    For each test, a project output folder is created (inside output folder). In a real case
    scenario, the project input folder is the same for output but here we want to ensure that
    input folder remains untouched.
    """

    def setUp(self):
        self.app = PyControlAPI(init_logger=True)

        self._base_output = os.path.join(TEST_DIR_PATH, 'output')
        self._base_input = os.path.join(TEST_DIR_PATH, 'input')

        project_output_folder = os.path.join(self._base_output, "output_setup_run_1_session")
        project_template_folder = os.path.join(self._base_input, "input_setup_run_1_session")
        if os.path.exists(project_output_folder):
            shutil.rmtree(project_output_folder)
        # ensure that project template folder remains untouched
        shutil.copytree(os.path.abspath(project_template_folder), os.path.abspath(project_output_folder))

        project_name = "proj1"
        project_full_path = os.path.join(project_output_folder, project_name)

        # load project
        self.proj1 = self.app.load_project(project_full_path)

    def test_setup_run_1_session(self):
        experiment_name = next((experiment for experiment in self.proj1.experiments if experiment.name == "FOXp1_group_2"), None)
        self.assertIsNotNone(experiment_name)
        setup1 = next((setup for setup in experiment_name.setups if setup.setup_id == "1"), None)
        self.assertIsNotNone(setup1)

        try:
            setup1.run()

            self.assertEqual(len(setup1.running_sessions()), 1)

            for session in setup1.running_sessions():
                self.assertIsInstance(session, RunSession)

            for var in range(2):
                time.sleep(10)
                setup1.update_data()

            setup1.stop()

            for session in setup1.running_sessions():
                for message in session.messages_history:
                    print(message)

        except SerialException:
            app_logger.warning("This test needs pycontrol boards")


app_logger = logging.getLogger(__name__)
