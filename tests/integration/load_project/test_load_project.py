# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrol.api.models.test_state_machine

"""

import os
import shutil

from unittest import TestCase

from pycontrolapi.models.pycontrolapi import PyControlAPI
from pycontrolapi.exceptions.invalid_project import InvalidProjectError

__author__ = "Carlos Mão de Ferro"
__copyright__ = ""
__credits__ = "Carlos Mão de Ferro"
__license__ = "MIT"
__version__ = "0.0"
__maintainer__ = ["Ricardo Ribeiro", "Carlos Mão de Ferro"]
__email__ = ["ricardojvr at gmail.com", "cajomferro at gmail.com"]
__status__ = "Development"

TEST_DIR_PATH = os.path.dirname(os.path.abspath(__file__))


class LoadProject(TestCase):
    """ Tests update_path method"""

    def setUp(self):
        self.app = PyControlAPI()

        self._base_output = os.path.join(TEST_DIR_PATH, 'output')
        self._base_input = os.path.join(TEST_DIR_PATH, 'input')

    def clean_folder(self, folder_path):
        """ Remove all files in the folder """
        filelist = [f for f in os.listdir(folder_path)]
        for f in filelist:
            os.remove(f)

        self.assertFalse(os.listdir(path=folder_path))

    def test_load_tasks_ok(self):
        """ Load project with 2 tasks without problems"""
        project_template_folder = os.path.join(self._base_input, "load_modify_save_project")

        project_name = "proj1"
        project_full_path = os.path.join(project_template_folder, project_name)

        # load project
        proj = self.app.load_project(project_full_path)

        self.assertEqual(len(proj.tasks), 2)

    def test_load_invalid_project_folder(self):
        """ Try to load project but folder doesn't exist"""
        project_output_folder = os.path.join(self._base_output, "output_load_invalid_project_folder")
        project_template_folder = os.path.join(self._base_input, "load_invalid_project_folder")
        if os.path.exists(project_output_folder):
            shutil.rmtree(project_output_folder)
        # ensure that project template folder remains untouched
        shutil.copytree(os.path.abspath(project_template_folder), os.path.abspath(project_output_folder))

        project_name = "proj1"
        project_full_path = os.path.join(project_output_folder, project_name)

        # load project
        with self.assertRaises(InvalidProjectError) as err:
            proj = self.app.load_project(project_full_path)

        self.assertTrue("Project dir doesn't exist:" in str(err.exception))

    def test_load_missing_tasks_folder(self):
        """ Try to load project but missing tasks folder"""
        project_output_folder = os.path.join(self._base_output, "output_load_missing_tasks_folder")
        project_template_folder = os.path.join(self._base_input, "load_missing_tasks_folder")
        if os.path.exists(project_output_folder):
            shutil.rmtree(project_output_folder)
        # ensure that project template folder remains untouched
        shutil.copytree(os.path.abspath(project_template_folder), os.path.abspath(project_output_folder))

        project_name = "proj1"
        project_full_path = os.path.join(project_output_folder, project_name)

        # load project
        with self.assertRaises(InvalidProjectError) as err:
            proj = self.app.load_project(project_full_path)

        self.assertTrue("Tasks dir doesn't exist:" in str(err.exception))

    def test_load_missing_data_folder(self):
        """ Try to load project but missing data folder"""
        project_output_folder = os.path.join(self._base_output, "output_load_missing_data_folder")
        project_template_folder = os.path.join(self._base_input, "load_missing_data_folder")
        if os.path.exists(project_output_folder):
            shutil.rmtree(project_output_folder)
        # ensure that project template folder remains untouched
        shutil.copytree(os.path.abspath(project_template_folder), os.path.abspath(project_output_folder))

        project_name = "proj1"
        project_full_path = os.path.join(project_output_folder, project_name)

        # load project
        with self.assertRaises(InvalidProjectError) as err:
            proj = self.app.load_project(project_full_path)

        self.assertTrue("Data dir doesn't exist:" in str(err.exception))

    def test_load_missing_frameworks_folder(self):
        """ Try to load project but missing frameworks folder"""
        project_output_folder = os.path.join(self._base_output, "output_load_missing_frameworks_folder")
        project_template_folder = os.path.join(self._base_input, "load_missing_frameworks_folder")
        if os.path.exists(project_output_folder):
            shutil.rmtree(project_output_folder)
        # ensure that project template folder remains untouched
        shutil.copytree(os.path.abspath(project_template_folder), os.path.abspath(project_output_folder))

        project_name = "proj1"
        project_full_path = os.path.join(project_output_folder, project_name)

        # load project
        with self.assertRaises(InvalidProjectError) as err:
            proj = self.app.load_project(project_full_path)

        self.assertTrue("Frameworks dir doesn't exist:" in str(err.exception))

    def test_load_project_no_settings_found(self):
        """ Try to load project but settings file is not found """

        project_output_folder = os.path.join(self._base_output, "output_load_tasks_no_settings_found")
        project_template_folder = os.path.join(self._base_input, "load_tasks_no_settings_found")
        if os.path.exists(project_output_folder):
            shutil.rmtree(project_output_folder)
        # ensure that project template folder remains untouched
        shutil.copytree(os.path.abspath(project_template_folder), os.path.abspath(project_output_folder))

        project_name = "proj1"
        project_full_path = os.path.join(project_template_folder, project_name)

        # load project
        with self.assertRaises(InvalidProjectError) as err:
            self.app.load_project(project_full_path)

        self.assertTrue("This file was not found: " in str(err.exception))

    def test_load_project_malformed_json(self):
        """ Try to load project but settings file is a malformed json """
        project_template_folder = os.path.join(self._base_input, "load_tasks_malformed_json")

        project_name = "proj1"
        project_full_path = os.path.join(project_template_folder, project_name)

        # load project
        with self.assertRaises(InvalidProjectError) as err:
            self.app.load_project(project_full_path)

        self.assertEqual(str(err.exception), "Malformed settings file: 'Expecting value: line 25 column 9 (char 648)'")

    def test_load_project_key_error_json(self):
        """ Try to load project but settings file is missing a key """
        project_template_folder = os.path.join(self._base_input, "load_tasks_key_error_json")

        project_name = "proj1"
        project_full_path = os.path.join(project_template_folder, project_name)

        # load project
        with self.assertRaises(InvalidProjectError) as err:
            self.app.load_project(project_full_path)

        self.assertEqual(str(err.exception), "Cannot find key in settings file: 'subjects'")

    def test_load_modify_save_project(self):
        """ Load, modify and save a project without problems"""
        project_output_folder = os.path.join(self._base_output, "output_load_modify_save")
        project_template_folder = os.path.join(self._base_input, "load_modify_save_project")
        if os.path.exists(project_output_folder):
            shutil.rmtree(project_output_folder)
        # ensure that project template folder remains untouched
        shutil.copytree(os.path.abspath(project_template_folder), os.path.abspath(project_output_folder))

        project_name = "proj1"
        project_full_path = os.path.join(project_output_folder, project_name)

        # load project
        proj = self.app.load_project(project_full_path)

        self.assertEqual(len(proj.experiments), 2)
        self.assertEqual(len(proj.subjects), 7)
        self.assertEqual(len(proj.boards), 3)

        experiment1 = next((experiment for experiment in proj.experiments if experiment.name == "FOXp1_group_1"), None)
        experiment_none = next((experiment for experiment in proj.experiments if experiment.name == "FOXp1_group_3"), None)
        self.assertIsNotNone(experiment1)
        self.assertIsNone(experiment_none)

        self.assertEqual(len(experiment1.setups), 2)
        setup1 = next((setup for setup in experiment1.setups if setup.setup_id == "1"), None)
        self.assertIsNotNone(setup1)
        setup2 = next((setup for setup in experiment1.setups if setup.setup_id == "2"), None)
        self.assertIsNotNone(setup2)
        setup3 = next((setup for setup in experiment1.setups if setup.setup_id == "3"), None)
        self.assertIsNone(setup3)

        self.assertTrue(len(setup1.boards) == 2)
        board_task_1 = setup1.boards[0]
        self.assertEqual(board_task_1[0], "2")  # board_id
        self.assertEqual(board_task_1[1], "blinker")  # task
        self.assertEqual(len(setup1.sessions), 2)

        # now, modify some things
        for board in proj.boards:
            if board.board_id == "2":
                proj.remove_board(board.board_id)
            if board.board_id == "3":
                board.board_id = "2"

        # print("Project boards: {0}".format(proj.boards))

        # and finally save
        self.app.save_project(proj.name)
