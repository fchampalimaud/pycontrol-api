# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrol.api.models.test_state_machine

"""

import os
import shutil

from unittest import TestCase

from pycontrolapi.models.pycontrolapi import PyControlAPI

__author__ = "Carlos Mão de Ferro"
__copyright__ = ""
__credits__ = "Carlos Mão de Ferro"
__license__ = "MIT"
__version__ = "0.0"
__maintainer__ = ["Ricardo Ribeiro", "Carlos Mão de Ferro"]
__email__ = ["ricardojvr at gmail.com", "cajomferro at gmail.com"]
__status__ = "Development"

TEST_DIR_PATH = os.path.dirname(os.path.abspath(__file__))


class CreateProject(TestCase):
    """ Tests update_path method"""

    def setUp(self):
        self.project_parent_path = os.path.join(TEST_DIR_PATH, 'output')

        if not os.path.exists(self.project_parent_path):
            os.mkdir(self.project_parent_path)

        self.assertTrue(os.path.exists(self.project_parent_path))

        self.app = PyControlAPI()

    def test_create_and_save_project(self):
        project_name = "proj1"
        project_path = os.path.join(self.project_parent_path, project_name)

        # clean previous test outputs (if exist)
        if os.path.exists(project_path):
            shutil.rmtree(project_path)
        self.assertFalse(os.path.exists(project_path))

        # create new project
        proj = self.app.create_project(project_path)

        self.assertTrue(os.path.exists(proj.path))

        exp1 = proj.add_experiment("FOXp1_group_1")
        exp2 = proj.add_experiment("FOXp2_group_1")

        sub1_exp1 = proj.add_subject("m129")
        sub2_exp1 = proj.add_subject("m130")
        sub1_exp2 = proj.add_subject("m291")
        proj.add_subject("m539")
        proj.add_subject("m937")
        proj.add_subject("m411")

        self.assertTrue(len(proj.subjects) == 6)

        board1 = proj.add_board("1", "pyboard", "/dev/usbmodem611")
        board2 = proj.add_board("2", "pyboard", "/dev/usbmodem281")
        board3 = proj.add_board("3", "pyboard", "/dev/usbmodem439")

        task1 = proj.add_task("blinker")
        task2 = proj.add_task("poke")

        setup1_subjects = []
        setup1_subjects.append(sub1_exp1)
        setup1_subjects.append(sub2_exp1)

        setup2_subjects = []
        setup2_subjects.append(sub1_exp2)

        setup1_exp1 = exp1.add_setup("1", ["m129", "m130", "m291"], [["2", "blinker"], ["3", "poke"]])

        self.assertEqual(setup1_exp1.setup_id, "1")
        self.assertTrue(len(setup1_exp1.subjects) == 3)
        self.assertTrue(len(setup1_exp1.boards) == 2)

        setup2_exp1 = exp1.add_setup("2", ["m291", "m999"], [["1", "blinker"], ["3", "poke"]])

        self.assertEqual(setup2_exp1.setup_id, "2")
        self.assertTrue(len(setup2_exp1.subjects) == 2)
        self.assertTrue(len(setup2_exp1.boards) == 2)

        self.assertTrue(len(proj.subjects) == 7)
        self.assertTrue(next((subject.subject_id for subject in proj.subjects if subject.subject_id == "m999"), None) is not None)

        proj.save()
