# !/usr/bin/python3
# -*- coding: utf-8 -*-
# pylint: disable=protected-access,pointless-statement
from pycontrolapi.models.board.com.events_list import EventsList
from pycontrolapi.models.board.com.states_list import StatesList
from pycontrolapi.models.board.com.event_fired import EventFired
from pycontrolapi.models.board.com.state_entry import StateEntry

""" pycontrolapi.models.session.test_session

"""

import tempfile
import os
import logging
from datetime import datetime

from unittest import TestCase
from unittest.mock import Mock
from unittest.mock import patch

from pycontrolapi.models.session.session import Session
from pycontrolapi.models.session.run_session import RunSession
from pycontrolapi.models.board.board import Board

__author__ = "Carlos Mão de Ferro and Ricardo Ribeiro"
__copyright__ = ""
__cred_its__ = "Carlos Mão de Ferro and Ricardo Ribeiro"
__license__ = "MIT"
__version__ = "0.0"
__maintainer__ = ["Ricardo Ribeiro", "Carlos Mão de Ferro"]
__email__ = ["ricardojvr at gmail.com", "cajomferro at gmail.com"]
__status__ = "Development"
__updated__ = "2015-11-12"

TEST_DIR_PATH = os.path.dirname(os.path.abspath(__file__))
APP_LOGGER = logging.getLogger(__name__)


class TestSessionInitialization(TestCase):
    """ Tests init and main properties
    WARNING: this test may fail if datetime retrievement lasts more than 1 sec (file names would be different then)
    """

    def setUp(self):
        self.experiment_name = "FOXp1_group_2"
        temp_setup_path = tempfile.TemporaryDirectory().name
        self.setup = Mock(setup_id="1", setup_path=temp_setup_path)
        self.mock_task = Mock(name="blinker", path="path_to_task")
        self.mock_board = Mock(board_id="1", serial_port="/dev/tty.usbmodem1462")
        self.subjects_ids = ["m349", "m492"]

    def test_session_file_path(self):
        """ Test if session file path is correctly set """
        session = Session(self.experiment_name, self.setup.setup_id, self.setup.setup_path, self.mock_board.board_id, self.mock_board.serial_port, self.mock_task, self.subjects_ids)
        session_file_name = "session_{0}_boardID{1}.txt".format(datetime.now().strftime('%Y%m%d_%H%M%S'), "1")
        expected_path = os.path.join(self.setup.setup_path, session_file_name)
        self.assertEqual(session.session_file_path, expected_path)

    def test_name(self):
        """ Test if session name is correctly returned """
        session = Session(self.experiment_name, self.setup.setup_id, self.setup.setup_path, self.mock_board.board_id, self.mock_board.serial_port, self.mock_task, self.subjects_ids)
        now_string = datetime.now().strftime('%Y%m%d_%H%M%S')
        board_id = 1
        expected_name = "session_{0}_boardID{1}".format(now_string, board_id)
        self.assertEqual(session.name, expected_name)


# class TestRunSessionFileOperations(TestCase):
#     """ Tests _save_message"""
#
#     def setUp(self):
#         self.temp_setup_path = tempfile.TemporaryDirectory()
#         self.setup = Mock(setup_id="1", setup_path="")
#         self.mock_task = Mock(name="blinker", path="path_to_task")
#         self.mock_board = Mock(board_id="1", serial_port="/dev/tty.usbmodem1462")
#         self.subjects = ["m349", "m492"]
#
#     def test_open_data_file(self):
#         "Tests if data file was correctly created"
#         session = RunSession(self.mock_board, self.temp_setup_path.name, self.setup.setup_id, self.setup.setup_path, self.mock_task, self.subjects)
#
#         self.assertFalse(os.path.exists(session.session_file_path))
#         with self.assertRaises(AttributeError):
#             session._data_file
#         session._open_data_file()
#         self.assertTrue(os.path.exists(session.session_file_path))
#         self.assertIsNotNone(session._data_file)
#
#     def test_open_write_close_data_file(self):
#         "Tests if data file was correctly created"
#
#         session = RunSession(self.mock_board, self.temp_setup_path.name, self.mock_task, self.subjects)
#
#         session._open_data_file()
#         session._data_file.write("simple test")
#         session._close_data_file()
#
#         read_file = open(session.session_file_path, "r")
#
#         self.assertEqual("simple test", read_file.readline())
#
#
# class MockRunSessionDoNotWork(RunSession):
#
#     def _do_work(self):
#         pass
#
#
# class TestRunSessionRun(TestCase):
#
#     def setUp(self):
#         self.temp_setup_path = tempfile.TemporaryDirectory()
#         self.setup = Mock(setup_id="1", setup_path="")
#         self.mock_task = Mock(name="blinker", path="path_to_task")
#         self.mock_board = Mock(board_id="1", serial_port="/dev/tty.usbmodem1462")
#         self.subjects = ["m349", "m492"]
#
#     def test_run_do_not_work(self):
#         session = MockRunSessionDoNotWork(self.mock_board, self.temp_setup_path.name, self.mock_task, self.subjects)
#
#         my_time_now = datetime.now()
#
#         session.run()
#
#         self.assertTrue(session.start_timestamp > my_time_now)  # session run set start timestamp after my_time_now
#
#     def test_run_save_start_timestamp(self):
#         """ Tests if start timestamp is saved to session file and to the messages history"""
#         session = MockRunSessionDoNotWork(self.mock_board, self.temp_setup_path.name, self.mock_task, self.subjects)
#         session._do_work = session._save_start_timestamp
#         self.assertTrue(len(session.messages_history) == 0)
#         session.run()
#
#         read_file = open(session.session_file_path, "r")
#         line1, line2, line3 = read_file.readlines()
#         self.assertEqual(line1, "Job started on board: 1\n")  # file has new line
#         self.assertTrue("START_TIMESTAMP:" in line2)
#         self.assertEqual(line3, "Job successfully finished\n")  # file has new line
#
#         self.assertTrue(len(session.messages_history) == 3)
#         msg1, msg2, msg3 = session.messages_history
#         self.assertEqual(msg1.content, "Job started on board: 1")
#         self.assertTrue("START_TIMESTAMP:" in msg2.content)
#         self.assertEqual(msg3.content, "Job successfully finished")
#
#     def test_run_open_connection(self):
#         """" Tests start open_connection method invocation """
#         session = MockRunSessionDoNotWork(self.mock_board, self.temp_setup_path.name, self.mock_task, self.subjects)
#         session._do_work = session._board.open_connection
#         self.assertTrue(len(session.messages_history) == 0)
#         session.run()
#
#     def test_run_start_framework(self):
#         """" Tests start_framework method invocation """
#         session = MockRunSessionDoNotWork(self.mock_board, self.temp_setup_path.name, self.mock_task, self.subjects)
#         session._do_work = session._board.start_framework
#         self.assertTrue(len(session.messages_history) == 0)
#         session.run()
