# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrol.api.models.test_create_project

"""

import tempfile
import os

from unittest import TestCase
from unittest.mock import Mock
from unittest.mock import patch

from pycontrolapi.models.task import Task

__author__ = "Carlos Mão de Ferro"
__copyright__ = ""
__credits__ = "Carlos Mão de Ferro"
__license__ = "MIT"
__version__ = "0.0"
__maintainer__ = ["Ricardo Ribeiro", "Carlos Mão de Ferro"]
__email__ = ["ricardojvr at gmail.com", "cajomferro at gmail.com"]
__status__ = "Development"


class TestUpdateFilename(TestCase):
    """ Tests update_path method"""

    def setUp(self):
        self.test_tasks_path = tempfile.TemporaryDirectory()
        self.target_file_path = tempfile.NamedTemporaryFile(dir=self.test_tasks_path.name, suffix=".py", delete=False)

        mock_project = Mock(tasks_path=self.test_tasks_path.name)
        self.assertTrue(os.path.exists(self.target_file_path.name))
        base = os.path.basename(self.target_file_path.name)
        self.sm_window = Task(os.path.splitext(base)[0], mock_project)

    def test_path_changed(self):
        """ Test if filename changed for new_name"""
        self.assertTrue(os.path.exists(self.sm_window.path))
        new_name = "blinker"
        self.sm_window.update_path(new_name)
        self.assertEqual(new_name, self.sm_window.name)

    def test_file_deleted(self):
        """Tests case when file doesn't exist"""
        self.sm_window.path = "old name"
        self.assertFalse(os.path.exists(self.sm_window.path))
        new_name = "blinker"
        with self.assertRaises(FileNotFoundError):
            self.sm_window.update_path(new_name)
        self.assertNotEqual(new_name, self.sm_window.name)

    def tearDown(self):
        self.target_file_path.close()


class TestInit(TestCase):
    """ Tests for creating state machine"""

    def test_name_ok(self):
        """ Test if state machine returns its name """
        tasks_path = tempfile.TemporaryDirectory()
        mock_project = Mock(tasks_path=tasks_path.name)
        sm_window = Task("blinker", mock_project)
        self.assertEqual(sm_window.name, "blinker")

    def test_path_ok(self):
        """Test state machine init with name 'blinker' when state machines path is 'some_path/some_sub_path'"""
        tasks_path = tempfile.TemporaryDirectory()
        task_name = 'blinker'
        mock_project = Mock(tasks_path=tasks_path.name)
        sm_window = Task(task_name, mock_project)
        # Test if state machine created with name 'blinker' creates path correctly
        self.assertEqual(sm_window.path, os.path.join(tasks_path.name, "blinker.py"))


class TestDeleteFile(TestCase):
    """ Tests delete_file method"""

    def setUp(self):
        """ Create a project containing a state machine"""
        self.test_tasks_path = tempfile.TemporaryDirectory()
        self.target_file_path = tempfile.NamedTemporaryFile(dir=self.test_tasks_path.name, suffix=".py", delete=False)

        self.mock_project = Mock(tasks_path=self.test_tasks_path.name, tasks=[])
        self.assertTrue(os.path.exists(self.target_file_path.name))
        base = os.path.basename(self.target_file_path.name)
        self.task = Task(os.path.splitext(base)[0], self.mock_project)

        self.trash_path = os.path.join(self.test_tasks_path.name, "_trash")
        self.trash_file_path = os.path.join(self.trash_path, "{0}.py".format(self.task.name))

    def test_delete_file_folder_not_exists(self):
        """ Test if trash folder is created and filename was moved to it """

        # state machine exists and has a valid path
        self.assertTrue(os.path.exists(self.task.path))

        # trash folder shouldn't exist yet
        self.assertFalse(os.path.exists(self.trash_file_path))

        # now delete file (in fact, move it to a trash folder)
        self.task.delete_file()

        # assert state machine file no longer exists in its path
        self.assertFalse(os.path.exists(self.task.path))

        # assert "deleted file" is now on trash folder
        self.assertTrue(os.path.exists(self.trash_file_path))

    def test_delete_file_folder_exists(self):
        """ Test if filename was moved to already created trash folder """
        os.mkdir(self.trash_path)

        # state machine exists and has a valid path
        self.assertTrue(os.path.exists(self.task.path))

        # trash folder should already exist
        self.assertTrue(os.path.exists(self.trash_path))

        # now delete file (in fact, move it to a trash folder)
        self.task.delete_file()

        # assert state machine file no longer exists in its path
        self.assertFalse(os.path.exists(self.task.path))

        # assert "deleted file" is now on trash folder
        self.assertTrue(os.path.exists(self.trash_file_path))

#     def test_remove_task(self):
#         """ Test if state machine is removed from project"""
#
#         self.assertTrue(self.task in self.mock_project.tasks)
#
#         self.task.remove_task()
#
#         self.assertFalse(self.task in self.mock_project.tasks)

    def tearDown(self):
        self.target_file_path.close()


class TestAvailableTaskFiles(TestCase):
    """ Test available_task_files method"""

    @patch('os.listdir')
    def test_files_ok(self, mock_listdir):
        """ Test when folder have only files with .py extension"""
        mock_listdir.return_value = ['blinker1.py', 'blinker2.py', 'blinker3.py']
        self.assertEqual(Task.available_tasks_files('some_path'), ['blinker1', 'blinker2', 'blinker3'])
        mock_listdir.assert_called_with('some_path')

    @patch('os.listdir')
    def test_files_empty(self, mock_listdir):
        """ Test when folder is empty"""
        mock_listdir.return_value = []
        self.assertEqual(Task.available_tasks_files('some_path'), [])
        mock_listdir.assert_called_with('some_path')

    @patch('os.listdir')
    def test_files_other_extensions(self, mock_listdir):
        """
        Test when folder have files with different extensions
        Expects: only names of the files with .py extension (but without extension)
        """
        mock_listdir.return_value = ['blinker1.py', 'blinker2', 'blinker3.txt', 'blinker1', 'blinker1.pypy', 'blinker5.py']
        self.assertEqual(Task.available_tasks_files('some_path'), ['blinker1', 'blinker5'])
        mock_listdir.assert_called_with('some_path')
