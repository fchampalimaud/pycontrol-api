# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrolapi.models.board.workers.test_upload_task

"""

from unittest import TestCase
from unittest.mock import Mock
from unittest.mock import MagicMock

import logging
from serial.serialutil import SerialException

import pycontrolapi  # import order matters here for logger

from pycontrol.board.pycboard import Pycboard
from pycontrol.board.pycboard import PycboardError

from pycontrolapi.models.board.workers.sync_variables import SyncVariables
from pycontrolapi import conf as settings

__version__ = pycontrolapi.__version__
__author__ = pycontrolapi.__author__
__credits__ = pycontrolapi.__credits__
__license__ = pycontrolapi.__license__
__maintainer__ = pycontrolapi.__maintainer__
__email__ = pycontrolapi.__email__
__status__ = pycontrolapi.__status__
__updated__ = "2016-07-16"

settings.setup_default_logger("pycontrol", file_handler=False)
settings.setup_default_logger("pycontrolapi", file_handler=False)
settings.setup_default_logger("pycontrolapi-tests", file_handler=False)

BOARD_SERIAL_PORT = settings.TEST_SERIAL_PORT
FRAMEWORK_PATH = settings.TEST_FRAMEWORK_PATH
TASK_PATH = settings.TEST_TASK_PATH

logger = logging.getLogger("pycontrolapi-tests")


class TestSyncVariables(TestCase):
    """
    Test sync variables job
    """

    def test_sync_variables_serial_port_not_found(self):
        """
        Test sync task variables to board and serial port is not available
        """
        board_log_messages = []

        mock_task = MagicMock()
        mock_task.configure_mock(name="blinker", path=TASK_PATH)
        mock_task.__str__.return_value = 'blinker'

        mock_board = MagicMock()
        mock_board.configure_mock(name="board1", serial_port='/dev/tty.usbmodemXXXX')
        mock_board.__str__.return_value = 'board1'

        mock_board_task = Mock()
        mock_board_task.configure_mock(board=mock_board, task=mock_task, task_variables=[])

        job = SyncVariables(mock_board_task, board_log_messages)

        with self.assertRaises(SerialException):
            handle_job(job)

        logger.debug(board_log_messages)

        self.assertEqual(len(board_log_messages), 2)
        self.assertTrue("# Syncing events, states and task variables..." in board_log_messages[0])
        self.assertTrue("! ERROR ! [Errno 2] could not open port /dev/tty.usbmodemXXXX" in board_log_messages[1])

    def test_sync_variables_file_not_found(self):
        """
        Test sync task variables to board and task path does not exist
        WARNING: THIS TEST REQUIRES A BOARD CONNECTED ON PORT
        """
        board_log_messages = []

        mock_task = MagicMock()
        mock_task.configure_mock(name="blinker", path="xxx")
        mock_task.__str__.return_value = 'blinker'

        mock_board = MagicMock()
        mock_board.configure_mock(name="board1", serial_port=settings.TEST_SERIAL_PORT)
        mock_board.__str__.return_value = 'board1'

        reset_board_filesystem()  # if a corrupted framework exists on board, a different exception will raise

        mock_board_task = Mock(board=mock_board, task=mock_task, task_variables=[])

        job = SyncVariables(mock_board_task, board_log_messages)

        with self.assertRaises(FileNotFoundError) as err:
            handle_job(job)

        self.assertEqual(str(err.exception), "[Errno 2] No such file or directory: 'xxx'")

        logger.debug(board_log_messages)

        self.assertEqual(len(board_log_messages), 2)
        self.assertTrue("# Syncing events, states and task variables..." in board_log_messages[0])
        self.assertTrue("! ERROR ! [Errno 2] No such file or directory: 'xxx'" in board_log_messages[1])

    def test_sync_variables_without_framework(self):
        """
        Test scenario where task is uploaded for board where framework is not installed or is invalid
        WARNING: THIS TEST REQUIRES A BOARD CONNECTED ON PORT AND A VALID FRAMEWORK PATH
        """
        board_log_messages = []

        mock_task = MagicMock()
        mock_task.configure_mock(name="blinker", path=settings.TEST_TASK_PATH)
        mock_task.__str__.return_value = 'blinker'

        mock_board = MagicMock()
        mock_board.configure_mock(name="board1", serial_port=settings.TEST_SERIAL_PORT)
        mock_board.__str__.return_value = 'board1'

        mock_board_task = Mock(board=mock_board, task=mock_task, task_variables=[])

        reset_board_filesystem()

        self.assertFalse(len(board_log_messages))

        job = SyncVariables(mock_board_task, board_log_messages)

        with self.assertRaises(PycboardError) as assert_err:
            handle_job(job)

        expected_board_message = 'Traceback (most recent call last):\r\n  ' \
                                 'File "<stdin>", line 1, in <module>\r\n  ' \
                                 'File "blinker.py", line 1, in <module>\r\n' \
                                 'ImportError: no module named \'pyControl\'\r\n'

        self.assertEqual(expected_board_message, str(assert_err.exception.board_exception))

        expected_message = "Framework not installed"
        self.assertEqual(expected_message, str(assert_err.exception))

        logger.debug(board_log_messages)

        self.assertEqual(len(board_log_messages), 2)
        self.assertTrue("# Syncing events, states and task variables..." in board_log_messages[0])
        self.assertTrue("! ERROR ! Framework not installed" in board_log_messages[1])

    def test_sync_variables_name_not_found(self):
        board_log_messages = []

        mock_task = MagicMock()
        mock_task.configure_mock(name="blinker", path=settings.TEST_TASK_PATH)
        mock_task.__str__.return_value = 'blinker'

        mock_board = MagicMock()
        mock_board.configure_mock(name="board1", serial_port=settings.TEST_SERIAL_PORT)
        mock_board.__str__.return_value = 'board1'

        mock_var_invalid = MagicMock()
        mock_var_invalid.configure_mock(name='LED_z', variable_value=3)
        mock_var_invalid.__str__.return_value = 'LED_z'

        task_variables = []
        task_variables.append(mock_var_invalid)

        mock_board_task = Mock(board=mock_board, task=mock_task, task_variables=task_variables)

        reset_board_filesystem()

        upload_framework()

        self.assertFalse(len(board_log_messages))

        job = SyncVariables(mock_board_task, board_log_messages)

        with self.assertRaises(PycboardError) as assert_err:
            handle_job(job)

        expected_message = "Set variable aborted: variable name LED_z not found."
        self.assertEqual(expected_message, str(assert_err.exception))

        logger.debug(board_log_messages)

        self.assertEqual(len(board_log_messages), 3)
        self.assertTrue("# Syncing events, states and task variables..." in board_log_messages[0])
        self.assertTrue("# Syncing var LED_z: 3" in board_log_messages[1])
        self.assertTrue("! ERROR ! Set variable aborted: variable name LED_z not found." in board_log_messages[2])

    def test_sync_variables_name(self):
        board_log_messages = []

        mock_task = MagicMock()
        mock_task.configure_mock(name="blinker", path=settings.TEST_TASK_PATH)
        mock_task.__str__.return_value = 'blinker'

        mock_board = MagicMock()
        mock_board.configure_mock(name="board1", serial_port=settings.TEST_SERIAL_PORT)
        mock_board.__str__.return_value = 'board1'

        mock_var = MagicMock()
        mock_var.configure_mock(name='LED_n', variable_value=3)
        mock_var.__str__.return_value = 'LED_n'

        task_variables = []
        task_variables.append(mock_var)

        mock_board_task = Mock(board=mock_board, task=mock_task, task_variables=task_variables)

        reset_board_filesystem()

        upload_framework()

        self.assertFalse(len(board_log_messages))

        job = SyncVariables(mock_board_task, board_log_messages)

        handle_job(job)

        logger.debug(board_log_messages)

        self.assertEqual(len(board_log_messages), 6)
        self.assertTrue("# Syncing events, states and task variables..." in board_log_messages[0])
        self.assertTrue("# Syncing var LED_n: 3" in board_log_messages[1])
        self.assertTrue("? V {'LED_n' : 3}" in board_log_messages[2])
        self.assertTrue("? E {'timer_evt': 3}" in board_log_messages[3])
        self.assertTrue("? S {'LED_on': 1, 'LED_off': 2}" in board_log_messages[4])
        self.assertTrue("# Sync completed" in board_log_messages[5])


def handle_job(job):
    job.run()

    while job.is_running:
        job.process_messages()


# AUXILIARY METHODS

def reset_board_filesystem():
    pyc = Pycboard(serial_port=BOARD_SERIAL_PORT)
    pyc.open_connection()
    pyc.reset_filesystem()  # ensures board is empty
    pyc.close()


def upload_framework():
    pyc = Pycboard(serial_port=BOARD_SERIAL_PORT)
    pyc.open_connection()
    pyc.reset_filesystem()  # ensures board is empty
    pyc.upload_framework(FRAMEWORK_PATH)
    pyc.close()
