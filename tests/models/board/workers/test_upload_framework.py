# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrol.api.models.test_create_project

"""

import logging
from unittest import TestCase

import pycontrolapi
from pycontrolapi.models.board.workers.upload_framework import UploadFramework
from pycontrolapi import conf as settings

from serial.serialutil import SerialException

__version__ = pycontrolapi.__version__
__author__ = pycontrolapi.__author__
__credits__ = pycontrolapi.__credits__
__license__ = pycontrolapi.__license__
__maintainer__ = pycontrolapi.__maintainer__
__email__ = pycontrolapi.__email__
__status__ = pycontrolapi.__status__
__updated__ = "2016-07-16"

settings.setup_default_logger("pycontrolapi", file_handler=False)
settings.setup_default_logger("pycontrol", file_handler=False)
settings.setup_default_logger("pycontrolapi-tests", file_handler=False)

logger = logging.getLogger("pycontrolapi-tests")

BOARD_SERIAL_PORT = settings.TEST_SERIAL_PORT
FRAMEWORK_PATH = settings.TEST_FRAMEWORK_PATH
HW_DEFINITION_PATH = settings.TEST_HW_DEFINITION_PATH


class TestUploadFramework(TestCase):
    """ Tests update_path method"""

    def setUp(self):
        pass

    def test_upload_framework_serial_port_not_found(self):
        """
        Test upload framework to board and serial port is not available
        """
        board_log_messages = []
        job = UploadFramework("/dev/tty.usbmodemXXXX",
                              FRAMEWORK_PATH,
                              board_log_messages
                              )

        with self.assertRaises(SerialException):
            handle_job(job)

        logger.debug(board_log_messages)

        self.assertEqual(len(board_log_messages), 2)
        self.assertTrue("# Installing framework: " in board_log_messages[0])
        self.assertTrue("! ERROR ! " in board_log_messages[1])

    def test_upload_framework_file_not_found(self):
        """
        Test upload framework to board and framework path does not exist
        WARNING: THIS TEST REQUIRES A BOARD CONNECTED ON PORT
        """
        board_log_messages = []
        job = UploadFramework(BOARD_SERIAL_PORT,
                              "xxx",
                              board_log_messages
                              )

        with self.assertRaises(FileNotFoundError) as err:
            handle_job(job)

        self.assertEqual(str(err.exception), "[Errno 2] No such file or directory: 'xxx'")

        logger.debug(board_log_messages)

        self.assertEqual(len(board_log_messages), 2)
        self.assertTrue("# Installing framework: " in board_log_messages[0])
        self.assertTrue("! ERROR ! " in board_log_messages[1])

    def test_upload_framework(self):
        """
        Test upload framework to board
        WARNING: THIS TEST REQUIRES A BOARD CONNECTED ON PORT AND A VALID FRAMEWORK PATH
        """
        board_log_messages = []

        self.assertFalse(len(board_log_messages))

        job = UploadFramework(BOARD_SERIAL_PORT,
                              FRAMEWORK_PATH,
                              board_log_messages
                              )

        handle_job(job)

        logger.debug(board_log_messages)

        self.assertEqual(len(board_log_messages), 3)
        self.assertTrue("# Installing framework: " in board_log_messages[0])
        self.assertTrue("? I " in board_log_messages[1])
        self.assertTrue("# Installation completed: " in board_log_messages[2])

    def test_upload_framework_with_hw_definition(self):
        """
        Test upload framework to board
        WARNING: THIS TEST REQUIRES A BOARD CONNECTED ON PORT AND A VALID FRAMEWORK PATH
        """
        board_log_messages = []

        self.assertFalse(len(board_log_messages))

        job = UploadFramework(BOARD_SERIAL_PORT,
                              FRAMEWORK_PATH,
                              board_log_messages,
                              HW_DEFINITION_PATH
                              )

        handle_job(job)

        logger.debug(board_log_messages)

        self.assertEqual(len(board_log_messages), 4)
        self.assertTrue("# Installing framework: " in board_log_messages[0])
        self.assertTrue("? I " in board_log_messages[1])
        self.assertTrue("# Installing HW definition: " in board_log_messages[2])
        self.assertTrue("# Installation completed: " in board_log_messages[3])

    def test_upload_framework_with_hw_definition_not_found(self):
        """
        Test upload framework and hardware defition (that doesn't exists) to board
        WARNING: THIS TEST REQUIRES A BOARD CONNECTED ON PORT AND A VALID FRAMEWORK PATH
        """
        board_log_messages = []

        self.assertFalse(len(board_log_messages))

        job = UploadFramework(BOARD_SERIAL_PORT,
                              FRAMEWORK_PATH,
                              board_log_messages,
                              "xxx"  # THIS IS TO BE INVALID
                              )

        with self.assertRaises(FileNotFoundError) as err:
            handle_job(job)

        self.assertEqual(str(err.exception), "[Errno 2] No such file or directory: 'xxx'")

        logger.debug(board_log_messages)

        self.assertEqual(len(board_log_messages), 4)
        self.assertTrue("# Installing framework: " in board_log_messages[0])
        self.assertTrue("? I " in board_log_messages[1])
        self.assertTrue("# Installing HW definition: " in board_log_messages[2])
        self.assertTrue(" ! ERROR ! [Errno 2] No such file or directory: 'xxx'" in board_log_messages[3])


def handle_job(job):
    job.run()

    while job.is_running:
        job.process_messages()
