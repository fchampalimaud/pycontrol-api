# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrolapi.models.board.workers.test_upload_task

"""

from unittest import TestCase
from unittest.mock import Mock
from unittest.mock import MagicMock

import os
import logging
import timer
from serial.serialutil import SerialException

import pycontrolapi  # import order matters here for logger

from pycontrol.board.pycboard import Pycboard
from pycontrol.board.pycboard import PycboardError

from pycontrolapi.models.board.workers.run_task import RunTask
from pycontrolapi import conf as settings

__version__ = pycontrolapi.__version__
__author__ = pycontrolapi.__author__
__credits__ = pycontrolapi.__credits__
__license__ = pycontrolapi.__license__
__maintainer__ = pycontrolapi.__maintainer__
__email__ = pycontrolapi.__email__
__status__ = pycontrolapi.__status__
__updated__ = "2016-07-16"

settings.setup_default_logger("pycontrolapi", file_handler=False)
settings.setup_default_logger("pycontrolapi-test", file_handler=False)
settings.setup_default_logger("pycontrol", file_handler=False)

BOARD_SERIAL_PORT = settings.TEST_SERIAL_PORT
FRAMEWORK_PATH = settings.TEST_FRAMEWORK_PATH
TASK_PATH = settings.TEST_TASK_PATH
OUTPUT_PATH = settings.TEST_OUTPUT

logger = logging.getLogger("pycontrolapi-test")

JOB_TIMER = None


class TestRunTask(TestCase):
    """
    Test run task job
    """

    def test_run_task_serial_port_not_found(self):
        """
        Test run task to board and serial port is not available
        """
        board_log_messages = []

        # CREATE TASK
        mock_task = MagicMock()
        mock_task.configure_mock(name="blinker", path=TASK_PATH)
        mock_task.__str__.return_value = 'blinker'

        # CREATE BOARD
        mock_board = MagicMock()
        mock_board.configure_mock(name="board1", serial_port='/dev/tty.usbmodemXXXX')
        mock_board.__str__.return_value = 'board1'

        # CREATE TASK VARIABLE AND STORE IT ON A LIST OF TASK VARIABLES
        mock_var = MagicMock()
        mock_var.configure_mock(name='LED_n', variable_value=3)
        mock_var.__str__.return_value = 'LED_n'
        task_variables = []
        task_variables.append(mock_var)

        # CREATE SESSION
        session_file_path = os.path.join(OUTPUT_PATH, "session_20160719_114802_board1.txt")
        mock_session = MagicMock()
        mock_session.configure_mock(experiment_name="my_exp", setup_id="m210", session_file_path=session_file_path,
                                    board_name=mock_board.name, board_serial_port=mock_board.serial_port,
                                    task=mock_task, subjects_ids=["m210"])

        # CREATE BOARD TASK
        mock_board_task = Mock()
        mock_board_task.configure_mock(board=mock_board, task=mock_task, task_variables=task_variables)

        job = RunTask(mock_session, mock_board_task, board_log_messages)

        with self.assertRaises(SerialException):
            handle_job(job)

        # REMOVE SESSION FILE
        os.remove(session_file_path)

        logger.debug(board_log_messages)

        self.assertEqual(len(board_log_messages), 2)
        self.assertTrue("# Running task: blinker" in board_log_messages[0])
        self.assertTrue("! ERROR ! [Errno 2] could not open port /dev/tty.usbmodemXXXX" in board_log_messages[1])

    def test_run_task_file_not_found(self):
        """
        Test run task to board and task path does not exist
        WARNING: THIS TEST REQUIRES A BOARD CONNECTED ON PORT
        """
        board_log_messages = []

        # CREATE TASK
        mock_task = MagicMock()
        mock_task.configure_mock(name="blinker", path="xxx")  # THIS IS TO BE INVALID
        mock_task.__str__.return_value = 'blinker'

        # CREATE BOARD
        mock_board = MagicMock()
        mock_board.configure_mock(name="board1", serial_port=BOARD_SERIAL_PORT)
        mock_board.__str__.return_value = 'board1'

        # CREATE TASK VARIABLE AND STORE IT ON A LIST OF TASK VARIABLES
        mock_var = MagicMock()
        mock_var.configure_mock(name='LED_n', variable_value=3)
        mock_var.__str__.return_value = 'LED_n'
        task_variables = []
        task_variables.append(mock_var)

        # CREATE SESSION
        session_file_path = os.path.join(OUTPUT_PATH, "session_20160719_114802_board1.txt")
        mock_session = MagicMock()
        mock_session.configure_mock(experiment_name="my_exp", setup_id="m210", session_file_path=session_file_path,
                                    board_name=mock_board.name, board_serial_port=mock_board.serial_port,
                                    task=mock_task, subjects_ids=["m210"])

        # CREATE BOARD TASK
        mock_board_task = Mock()
        mock_board_task.configure_mock(board=mock_board, task=mock_task, task_variables=task_variables)

        job = RunTask(mock_session, mock_board_task, board_log_messages)

        with self.assertRaises(FileNotFoundError) as err:
            handle_job(job)

        self.assertEqual(str(err.exception), "[Errno 2] No such file or directory: 'xxx'")

        # REMOVE SESSION FILE
        os.remove(session_file_path)

        logger.debug(board_log_messages)

        self.assertEqual(len(board_log_messages), 2)
        self.assertTrue("# Running task: blinker" in board_log_messages[0])
        self.assertTrue("! ERROR ! [Errno 2] No such file or directory: 'xxx'" in board_log_messages[1])

    def test_run_session_file_not_found(self):
        """
        Test run task on board and session file path does not exist
        WARNING: THIS TEST REQUIRES A BOARD CONNECTED ON PORT
        """
        board_log_messages = []

        # CREATE TASK
        mock_task = MagicMock()
        mock_task.configure_mock(name="blinker", path=TASK_PATH)
        mock_task.__str__.return_value = 'blinker'

        # CREATE BOARD
        mock_board = MagicMock()
        mock_board.configure_mock(name="board1", serial_port=BOARD_SERIAL_PORT)
        mock_board.__str__.return_value = 'board1'

        # CREATE TASK VARIABLE AND STORE IT ON A LIST OF TASK VARIABLES
        mock_var = MagicMock()
        mock_var.configure_mock(name='LED_n', variable_value=3)
        mock_var.__str__.return_value = 'LED_n'
        task_variables = []
        task_variables.append(mock_var)

        # CREATE SESSION
        session_file_path = os.path.join("xxx", "session_20160719_114802_board1.txt")
        mock_session = MagicMock()
        mock_session.configure_mock(experiment_name="my_exp", setup_id="m210", session_file_path=session_file_path,
                                    board_name=mock_board.name, board_serial_port=mock_board.serial_port,
                                    task=mock_task, subjects_ids=["m210"])

        # CREATE BOARD TASK
        mock_board_task = Mock()
        mock_board_task.configure_mock(board=mock_board, task=mock_task, task_variables=task_variables)

        job = RunTask(mock_session, mock_board_task, board_log_messages)

        with self.assertRaises(FileNotFoundError) as err:
            handle_job(job)

        self.assertEqual(str(err.exception),
                         "[Errno 2] No such file or directory: 'xxx/session_20160719_114802_board1.txt'")

        # NO NEED TO REMOVE BECAUSE IT WAS NOT CREATED
        # os.remove(session_file_path)

        logger.debug(board_log_messages)

        self.assertEqual(len(board_log_messages), 2)
        self.assertTrue("# Running task: blinker" in board_log_messages[0])
        self.assertTrue("! ERROR ! [Errno 2] No such file or directory: 'xxx/session_20160719_114802_board1.txt'" in
                        board_log_messages[1])

    def test_run_task_without_framework(self):
        """
        Test scenario where task is uploaded for board where framework is not installed or is invalid
        WARNING: THIS TEST REQUIRES A BOARD CONNECTED ON PORT AND A VALID FRAMEWORK PATH
        """
        board_log_messages = []

        # CREATE TASK
        mock_task = MagicMock()
        mock_task.configure_mock(name="blinker", path=TASK_PATH)
        mock_task.__str__.return_value = 'blinker'

        # CREATE BOARD
        mock_board = MagicMock()
        mock_board.configure_mock(name="board1", serial_port=BOARD_SERIAL_PORT)
        mock_board.__str__.return_value = 'board1'

        # CREATE TASK VARIABLE AND STORE IT ON A LIST OF TASK VARIABLES
        mock_var = MagicMock()
        mock_var.configure_mock(name='LED_n', variable_value=3)
        mock_var.__str__.return_value = 'LED_n'
        task_variables = []
        task_variables.append(mock_var)

        # CREATE SESSION
        session_file_path = os.path.join(OUTPUT_PATH, "session_20160719_114802_board1.txt")
        mock_session = MagicMock()
        mock_session.configure_mock(experiment_name="my_exp", setup_id="m210", session_file_path=session_file_path,
                                    board_name=mock_board.name, board_serial_port=mock_board.serial_port,
                                    task=mock_task, subjects_ids=["m210"])

        # CREATE BOARD TASK
        mock_board_task = Mock()
        mock_board_task.configure_mock(board=mock_board, task=mock_task, task_variables=task_variables)

        job = RunTask(mock_session, mock_board_task, board_log_messages)

        self.assertFalse(len(board_log_messages))
        reset_board_filesystem()  # THIS IS THE TEST, CLEAR FILESYSTEM

        with self.assertRaises(PycboardError) as assert_err:
            handle_job(job)

        expected_board_message = 'Traceback (most recent call last):\r\n  ' \
                                 'File "<stdin>", line 1, in <module>\r\n  ' \
                                 'File "blinker.py", line 1, in <module>\r\n' \
                                 'ImportError: no module named \'pyControl\'\r\n'

        self.assertEqual(expected_board_message, str(assert_err.exception.board_exception))

        expected_message = "Framework not installed"
        self.assertEqual(expected_message, str(assert_err.exception))

        # REMOVE SESSION FILE
        os.remove(session_file_path)

        logger.debug(board_log_messages)

        self.assertEqual(len(board_log_messages), 2)
        self.assertTrue("# Running task: blinker" in board_log_messages[0])
        self.assertTrue("! ERROR ! Framework not installed" in board_log_messages[1])

    def test_run_task(self):
        """

        WARNING1: this test results depend on the task content, so ensure task file is appropriate
        WARNING2: sometimes the time library raise this error: Fatal Python error: take_gil: NULL tstate
        """
        board_log_messages = []

        # CREATE TASK
        mock_task = MagicMock()
        mock_task.configure_mock(name="blinker", path=TASK_PATH)
        mock_task.__str__.return_value = 'blinker'

        # CREATE BOARD
        mock_board = MagicMock()
        mock_board.configure_mock(name="board1", serial_port=BOARD_SERIAL_PORT)
        mock_board.__str__.return_value = 'board1'

        # CREATE TASK VARIABLE AND STORE IT ON A LIST OF TASK VARIABLES
        mock_var = MagicMock()
        mock_var.configure_mock(name='LED_n', variable_value=3)
        mock_var.__str__.return_value = 'LED_n'
        task_variables = []
        task_variables.append(mock_var)

        # CREATE SESSION
        session_file_path = os.path.join(OUTPUT_PATH, "session_20160719_114802_board1.txt")
        mock_session = MagicMock()
        mock_session.configure_mock(experiment_name="my_exp", setup_id="m210", session_file_path=session_file_path,
                                    board_name=mock_board.name, board_serial_port=mock_board.serial_port,
                                    task=mock_task, subjects_ids=["m210"])

        # CREATE BOARD TASK
        mock_board_task = Mock()
        mock_board_task.configure_mock(board=mock_board, task=mock_task, task_variables=task_variables)

        job = RunTask(mock_session, mock_board_task, board_log_messages)

        reset_board_filesystem()
        upload_framework()
        self.assertFalse(len(board_log_messages))

        handle_job(job)

        # KEEP SESSION FILE TO CONFIRM
        # os.remove(session_file_path)

        logger.debug(board_log_messages)

        self.assertTrue(len(board_log_messages) >= 10)
        self.assertTrue("# Running task: blinker" in board_log_messages[0])
        self.assertTrue("# Syncing var LED_n: 3" in board_log_messages[1])
        self.assertTrue("? V {'LED_n' : 3}" in board_log_messages[2])
        self.assertTrue("? E {'timer_evt': 3}" in board_log_messages[3])
        self.assertTrue("? S {'LED_on': 1, 'LED_off': 2}" in board_log_messages[4])
        self.assertTrue("? D 0 1" in board_log_messages[5])
        self.assertTrue("# Framework is now stopped" in board_log_messages[-4])
        self.assertTrue("# Printing task variables values" in board_log_messages[-3])
        self.assertTrue("? V {'LED_n' : 3" in board_log_messages[-2])
        self.assertTrue("# Run completed" in board_log_messages[-1])


def handle_job(job):
    """

    :param job:
    :return:
    """
    global JOB_TIMER
    job.run()

    JOB_TIMER = timer.Timer(5000000, job.stop)  # 5 second timer
    JOB_TIMER.start()

    while job.is_running:  # and JOB_TIMER_CONTROLLER:
        job.process_messages()


# AUXILIARY METHODS

def reset_board_filesystem():
    pyc = Pycboard(serial_port=BOARD_SERIAL_PORT)
    pyc.open_connection()
    pyc.reset_filesystem()  # ensures board is empty
    pyc.close()


def upload_framework():
    pyc = Pycboard(serial_port=BOARD_SERIAL_PORT)
    pyc.open_connection()
    pyc.reset_filesystem()  # ensures board is empty
    pyc.upload_framework(FRAMEWORK_PATH)
    pyc.close()
