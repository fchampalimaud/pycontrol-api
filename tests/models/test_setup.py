# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrol.api.models.test_create_project

"""

import tempfile
import time
import datetime
import os
import logging

from unittest import TestCase
from unittest.mock import Mock
from unittest.mock import MagicMock
from unittest.mock import patch

import pycontrolapi
from pycontrolapi.models.setup import Setup
from pycontrolapi.exceptions.entity_creation import EntityCreationError
from pycontrolapi.models.session.session import Session
from pycontrolapi import conf as settings

__author__ = pycontrolapi.__author__
__credits__ = pycontrolapi.__credits__
__license__ = pycontrolapi.__license__
__version__ = pycontrolapi.__version__
__maintainer__ = pycontrolapi.__maintainer__
__email__ = pycontrolapi.__email__
__status__ = pycontrolapi.__status__
__updated__ = "2016-07-22"

TEST_DIR_PATH = os.path.dirname(os.path.abspath(__file__))

settings.setup_default_logger("pycontrolapi", file_handler=False)
settings.setup_default_logger("pycontrolapi-tests", file_handler=False)
logger = logging.getLogger("pycontrolapi-tests")


class TestInit(TestCase):
    """ Tests for creating state machine"""

    def setUp(self):
        project = Mock(boards=[], tasks=[], subjects=[])
        self.experiment = Mock(project=project)

    def test_create_with_id(self):
        """ Test if setup id is well defined"""
        setup = Setup("1", experiment=self.experiment)
        self.assertEqual(setup.setup_id, "1")
        self.assertIsNotNone(setup.experiment)
        self.assertTrue(len(setup.subjects) == 0)
        self.assertTrue(len(setup.boards) == 0)
        self.assertTrue(len(setup.sessions) == 0)

    #     def test_create_with_fields_that_exist(self):
    #         """ Mock all entities that setup needs and assign some values"""
    #
    #         boards = []
    #         board1 = Mock(board_id="3")
    #         boards.append(board1)
    #         board2 = Mock(board_id="9")
    #         boards.append(board2)
    #
    #         task1 = Mock(name="blinker2", path="path_to_task/blinker2")
    #         tasks = []
    #         tasks.append(task1)
    #
    #         subject1 = Mock(subject_id="m918")
    #         subjects = []
    #         subjects.append(subject1)
    #
    #         project = Mock(boards=boards, tasks=tasks, subjects=subjects)
    #         experiment = Mock(project=project)
    #
    #         setup = Setup("7", subjects=["m918"], boards=[["3", "blinker2"], ["3", "blinker4"]], experiment=experiment)
    #         self.assertEqual(setup.setup_id, "7")
    #         self.assertTrue(len(setup.subjects) == 1)
    #         self.assertTrue(len(setup.boards) == 1)
    #         self.assertEqual(setup.boards[0][0], "4")
    #
    #         # TODO: this is not working, maybe because task's name is a property that depends on the path
    #         #self.assertEqual(setup.task._name, "blinker2")
    #
    #         self.assertEqual(setup.subjects[0], "m918")
    #
    #         self.assertFalse(setup.sessions)

    def test_create_with_repeated_boards(self):
        """ Test if repeated boards ids are not added twice and last task is assigned"""

        setup = Setup("7", subjects=["m918"],
                      boards_tasks=[["3", "blinker2"], ["3", "blinker4"], ["3", "blinker7"], ["4", "blinker1"]],
                      experiment=self.experiment)
        self.assertEqual(setup.setup_id, "7")
        self.assertTrue(len(setup.subjects) == 1)
        self.assertTrue(len(setup.boards) == 2)
        self.assertEqual(setup.boards[0][0], "3")
        self.assertEqual(setup.boards[0][1], "blinker7")
        self.assertEqual(setup.boards[1][0], "4")
        self.assertEqual(setup.boards[1][1], "blinker1")

        self.assertEqual(setup.subjects[0], "m918")

        self.assertFalse(setup.sessions)

    def test_experiment_is_none(self):
        """ Test if setup receives a valid experiment upon creation """
        with self.assertRaises(EntityCreationError) as exc:
            setup = Setup("1")


class TestAssignBoardTask(TestCase):
    def setUp(self):
        project = Mock(boards=[], tasks=[], subjects=[])
        self.experiment = Mock(project=project)

    def test_assign_board_task_that_dont_exist(self):
        # init setup with some subjects and boards
        setup = Setup("7", subjects=["m918"], boards=[["3", "blinker2"], ["4", "blinker2"]], experiment=self.experiment)

        setup.assign_board_task("1", "blinker9")

        self.assertTrue(len(setup.boards) == 3)

        self.assertTrue(setup.boards[2][0], "1")
        self.assertTrue(setup.boards[2][1], "blinker9")


class TestSessionLoadFromLogFile(TestCase):
    """ Tests method load_from_log_file"""

    def setUp(self):
        project = Mock(boards=[], tasks=[], subjects=[])
        self.experiment = Mock(project=project, name="FOXp1_group_2",
                               experiment_path=os.path.join(TEST_DIR_PATH, "templates"))

        self.task = Mock(name="blinker2", path="xxx/yyy/zzz/blinker2.py")
        self.setup = Setup("1", experiment=self.experiment)
        self.board = Mock(board_id="1", serial_port="/dev/tty.usbmodem1472")
        self.subjects_ids = ["m129", "m291"]

    def test_valid(self):
        session = Session(self.experiment.experiment_name, self.setup.setup_id, self.setup.setup_path,
                          self.board.board_id, self.board.serial_port, self.task.name, self.task.path,
                          self.subjects_ids, "session_20151110_161039_boardID1.txt")
        self.assertTrue(len(session.messages_history) == 0)
        self.setup.load_session_history_from_file(self.task, session)
        self.assertTrue(len(session.messages_history) == 12)
        for message in session.messages_history:
            logger.debug(str(message))
        self.assertEqual(str(session.messages_history[0])[17:], "? E {'timer_evt': 3}")
        self.assertEqual(str(session.messages_history[1])[17:], "? S {'LED_on': 1, 'LED_off': 2}")
        self.assertEqual(str(session.messages_history[6])[17:], "? D 2000 0 3")
        self.assertEqual(str(session.messages_history[11])[17:], "? D 4301 0 1")


class TestRestoreTaskVariables(TestCase):
    def test_restore_task_variables_from_session_empty_list(self):
        """

        """

        experiment = MagicMock()
        setup = Setup("m122", subjects=[], boards_tasks=[], experiment=experiment)

        setup.sessions = []

        self.assertIsNotNone(setup.sessions)

        last_session = setup.get_most_recent_session()

        self.assertIsNone(last_session)

    def test_restore_task_variables_from_session(self):
        """

        """

        experiment = MagicMock()
        setup = Setup("m122", subjects=[], boards_tasks=[], experiment=experiment)

        setup.sessions = []

        session1 = MagicMock()
        session1.configure_mock(order_id=1, end_date=datetime.datetime.now())

        time.sleep(2)  # waits 2 seconds

        session2 = MagicMock()
        session2.configure_mock(order_id=2, end_date=datetime.datetime.now())

        time.sleep(2)  # waits 2 seconds

        session3 = MagicMock()
        session3.configure_mock(order_id=3, end_date=datetime.datetime.now())

        # mix order
        setup.sessions.append(session2)
        setup.sessions.append(session3)
        setup.sessions.append(session1)

        self.assertIsNotNone(setup.sessions)

        last_session = setup.get_most_recent_session()

        logger.debug([(session.order_id, session.end_date) for session in setup.sessions])
        self.assertEqual(1, last_session.order_id)
