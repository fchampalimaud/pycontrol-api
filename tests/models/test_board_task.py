'''
Created on 17/06/2016

@author: carlos
'''

from unittest import TestCase
from unittest.mock import Mock
from pycontrolapi.models.board_task import BoardTask


class TestInit(TestCase):
    """ Tests for creating state machine"""

    #     def test_compare_board_task(self):
    #         board1 = Mock(board_name="board1")
    #         task1 = Mock(name="blinker1")
    #
    #         board_task1 = BoardTask(board1, task1)
    #         board_task2 = BoardTask(board1, task1)
    #
    #         self.assertNotEqual(str(board_task1), str(board_task2))
    #
    #         self.assertEqual(board_task1, board_task2)

    def test_compare_board_task_in_list(self):
        board1 = Mock(board_name="board1")
        task1 = Mock(name="blinker1")

        board_task1 = BoardTask(board1, task1)
        board_task2 = BoardTask(board1, task1)

        boards_tasks_list = []
        boards_tasks_list.append(board_task1)

        self.assertTrue(board_task1 in boards_tasks_list)
        self.assertTrue(board_task2 in boards_tasks_list)

    def test_compare_board_task_same_board_same_task(self):
        board1 = Mock(name="board1")
        task1 = Mock(name="blinker1")

        board_task1 = BoardTask(board1, task1)
        board_task2 = BoardTask(board1, task1)

        self.assertTrue(board_task1 == board_task2)

    def test_compare_board_task_board_empty(self):
        board1 = Mock(name="board1")
        task1 = Mock(name="blinker1")

        board_task1 = BoardTask(None, task1)
        board_task2 = BoardTask(board1, task1)

        self.assertFalse(board_task1 == board_task2)

    def test_compare_board_task_task_empty(self):
        board1 = Mock(name="board1")
        task1 = Mock(name="blinker1")

        board_task1 = BoardTask(board1, None)
        board_task2 = BoardTask(board1, task1)

        self.assertFalse(board_task1 == board_task2)

    def test_compare_board_task_boards_empty_same_task(self):
        task1 = Mock(name="blinker1")

        board_task1 = BoardTask(None, task1)
        board_task2 = BoardTask(None, task1)

        self.assertTrue(board_task1 == board_task2)

    def test_compare_board_task_tasks_empty_same_board(self):
        board1 = Mock(name="board1")

        board_task1 = BoardTask(board1, None)
        board_task2 = BoardTask(board1, None)

        self.assertTrue(board_task1 == board_task2)

    def test_compare_board_task_boards_empty_different_task(self):
        task1 = Mock(name="blinker1")
        task2 = Mock(name="blinker2")

        board_task1 = BoardTask(None, task1)
        board_task2 = BoardTask(None, task2)

        self.assertFalse(board_task1 == board_task2)

    def test_compare_board_task_tasks_empty_different_board(self):
        board1 = Mock(name="board1")
        board2 = Mock(name="board2")

        board_task1 = BoardTask(board1, None)
        board_task2 = BoardTask(board2, None)

        self.assertFalse(board_task1 == board_task2)

    def test_compare_board_task_all_empty(self):
        board_task1 = BoardTask(None, None)
        board_task2 = BoardTask(None, None)

        self.assertTrue(board_task1 == board_task2)
