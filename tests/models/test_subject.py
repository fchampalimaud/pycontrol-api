# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrol.api.models.test_subject

"""

from unittest import TestCase
from unittest.mock import Mock

from pycontrolapi.models.subject import Subject

__author__ = "Carlos Mão de Ferro"
__copyright__ = ""
__credits__ = "Carlos Mão de Ferro"
__license__ = "MIT"
__version__ = "0.0"
__maintainer__ = ["Ricardo Ribeiro", "Carlos Mão de Ferro"]
__email__ = ["ricardojvr at gmail.com", "cajomferro at gmail.com"]
__status__ = "Development"


class TestInit(TestCase):
    """ Tests related to object creation """

    def test_name_OK(self):
        """ Test if subject get its id defined upon creation"""
        mock_project = Mock()
        subject = Subject("m239", mock_project)
        self.assertEqual(subject.subject_id, "m239")
