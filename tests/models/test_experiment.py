# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrol.api.models.test_experiment

"""

from unittest import TestCase
from unittest.mock import Mock

import pycontrolapi
from pycontrolapi.models.experiment import Experiment

__version__ = pycontrolapi.__version__
__author__ = pycontrolapi.__author__
__credits__ = pycontrolapi.__credits__
__license__ = pycontrolapi.__license__
__maintainer__ = pycontrolapi.__maintainer__
__email__ = pycontrolapi.__email__
__status__ = pycontrolapi.__status__
__updated__ = "2016-07-14"


class TestExperiment(TestCase):

    def test_name_OK(self):
        mock_project = Mock()
        experiment = Experiment("FOXp2_group_1", mock_project)
        self.assertEqual(experiment.name, "FOXp2_group_1")

#     def test_task_OK(self):
#         mock_project = Mock()
#         mock_state_machine = Mock()
#         mock_state_machine.name = "blinker"
#         experiment = Experiment("FOXp2_group_1", "", mock_state_machine, mock_project)
#         self.assertEqual(experiment.state_machine.name, "blinker")

#     def test_folder_name_OK(self):
#         mock_project = Mock()
#         experiment = Experiment("FOXp2_group_1", mock_project)
#         self.assertEqual(experiment._folder, "2015-10-09_FOXp2_group_1")
