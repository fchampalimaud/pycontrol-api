'''
Created on 23/07/2015

@author: carlos
'''

__author__ = "Carlos Mão de Ferro and Ricardo Ribeiro"
__copyright__ = ""
__credits__ = "Carlos Mão de Ferro and Ricardo Ribeiro"
__license__ = "MIT"
__version__ = "0.0"
__maintainer__ = ["Ricardo Ribeiro", "Carlos Mão de Ferro"]
__email__ = ["ricardojvr at gmail.com", "cajomferro at gmail.com"]
__status__ = "Development"


JSON_PROJECT_ROOT = "project"
JSON_PROJECT_PATH = "path"

JSON_PROJECT_EXPERIMENTS = "experiments"
JSON_PROJECT_EXPERIMENT_NAME = "name"
JSON_PROJECT_EXPERIMENT_STARTDATE = "start_date"
JSON_PROJECT_EXPERIMENT_SETUPS = "setups"

JSON_PROJECT_SUBJECTS = "subjects"
JSON_PROJECT_SUBJECT_SUBJECT_ID = "subject_id"

JSON_PROJECT_BOARDS = "boards"
JSON_PROJECT_BOARD_BOARD_NAME = "board_name"
JSON_PROJECT_BOARD_BOARD_ID = "board_id"
JSON_PROJECT_BOARD_SERIAL = "board_serial_port"
JSON_PROJECT_BOARD_FRAMEWORK_NAME = "framework_name"

JSON_PROJECT_SETUP_SETUP_ID = "setup_id"
JSON_PROJECT_SETUP_SUBJECTS = "subjects"
JSON_PROJECT_SETUP_BOARDS_TASKS = "boards_tasks"
JSON_PROJECT_SETUP_BOARDS_BOARD_ID = "board_id"
JSON_PROJECT_SETUP_BOARDS_TASK_NAME = "task_name"

JSON_PROJECT_BOARD_TASK_VARIABLES = "variables"
JSON_PROJECT_BOARD_TASK_BOARD = 'board'
JSON_PROJECT_BOARD_TASK_TASK = 'task'

JSON_PROJECT_TASK_VARIABLES_NAME 		= "variable_name"
JSON_PROJECT_TASK_VARIABLES_VALUE 		= "variable_value"
JSON_PROJECT_TASK_VARIABLES_PERSISTENT 	= "variable_persistent"

SESSION_FILE_EXTENSION = ".txt"
TASK_FILE_EXTENSION = ".py"
STATEMACHINE_FILE_EVENTS = "events"
STATEMACHINE_FILE_STATES = "states"

PROJECT_SETTINGS_TASKS_DIR = "tasks"
PROJECT_SETTINGS_DATA_DIR = "data"
PROJECT_SETTINGS_FRAMEWORKS_DIR = "boards"
PROJECT_SETTINGS_FILENAME = "settings.json"

PYCONTROL_FRAMEWORK_FOLDER = "pyControl"
PYCONTROL_FRAMEWORK_HW_DESCRIPTION = "hardware_definition.py"