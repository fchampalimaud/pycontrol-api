import sys
from pycontrolapi.models.project import Project

def create_project():
	if len(sys.argv)>1:
		proj1 = Project()
		proj1.name = "Undefined project"
		proj1.save(sys.argv[1])
	else:
		print('Please set the project path parameter')