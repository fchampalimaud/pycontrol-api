# !/usr/bin/python3
# -*- coding: utf-8 -*-

import logging
import loggingbootstrap

try:
	from pysettings import conf

	# Initiating logging for pysettings. It has to be initiated manually here because we don't know yet
	# the logger filename as specified on settings
	loggingbootstrap.create_double_logger("pysettings", logging.INFO, 'pycontrolapi.log', logging.INFO)

except ImportError as err:
	logging.getLogger().critical(str(err), exc_info=True)
	exit("Could not load pysettings! Is it installed?")

import pycontrolapi  # load settings

loggingbootstrap.create_double_logger("pycontrolapi", conf.APP_LOG_HANDLER_CONSOLE_LEVEL, conf.APP_LOG_FILENAME,
                                      conf.APP_LOG_HANDLER_FILE_LEVEL)

logger = logging.getLogger("pycontrolapi")

# try loggers
logger.debug("Logger for pycontrol-api loaded (DEBUG)...")
logger.info("Logger for pycontrol-api loaded (INFO)...")
