# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrolapi.exceptions.invalid_project

"""


from pycontrolapi.exceptions.pycontrol_api_exception import PyControlAPIException

__author__ = "Carlos Mão de Ferro and Ricardo Ribeiro"
__copyright__ = ""
__cred_its__ = "Carlos Mão de Ferro and Ricardo Ribeiro"
__license__ = "MIT"
__version__ = "0.0"
__maintainer__ = ["Ricardo Ribeiro", "Carlos Mão de Ferro"]
__email__ = ["ricardojvr at gmail.com", "cajomferro at gmail.com"]
__status__ = "Development"
__updated__ = "2015-11-05"


class InvalidProjectError(PyControlAPIException):
    """ Exception raised when an invalid project is loaded"""

    def __init__(self, value, original_exception=None):
        self.value = value
        self.original_exception = original_exception

    def __str__(self):
        return self.value
