# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pycontrolapi.exceptions.pycontrol_api_exception import PyControlAPIException


class SetupJobStopped(PyControlAPIException):
	""" Exception raised when setup process has finished"""

	def __init__(self, value=""):
		self.value = value

	def __str__(self):
		return self.value
