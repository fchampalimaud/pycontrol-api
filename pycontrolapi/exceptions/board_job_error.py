# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pycontrolapi.exceptions.pycontrol_api_exception import PyControlAPIException


class BoardJobError(PyControlAPIException):
	""" Exception raised when a task is run but some problem occurs"""

	def __init__(self, value, original_exception=None):
		self.value = value
		self.original_exception = original_exception

	def __str__(self):
		return self.value
