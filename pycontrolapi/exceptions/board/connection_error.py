# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pycontrolapi.exceptions.board_job_error import BoardJobError


class BoardConnectionError(BoardJobError):
	""" Exception raised when a task is run but some problem occurs"""

	def __init__(self, value, original_exception=None):
		BoardJobError.__init__(self, value, original_exception)
