# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrolapi.exceptions.duplicate_entity

"""


from pycontrolapi.exceptions.pycontrol_api_exception import PyControlAPIException

__author__ = "Carlos Mão de Ferro and Ricardo Ribeiro"
__copyright__ = ""
__cred_its__ = "Carlos Mão de Ferro and Ricardo Ribeiro"
__license__ = "MIT"
__version__ = "0.0"
__maintainer__ = ["Ricardo Ribeiro", "Carlos Mão de Ferro"]
__email__ = ["ricardojvr at gmail.com", "cajomferro at gmail.com"]
__status__ = "Development"
__updated__ = "2016-03-08"


class EntityCreationError(PyControlAPIException):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return self.value
