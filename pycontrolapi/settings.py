# !/usr/bin/python3
# -*- coding: utf-8 -*-

import logging

APP_LOG_FILENAME = 'pycontrolapi.log'
APP_LOG_HANDLER_FILE_LEVEL = logging.DEBUG
APP_LOG_HANDLER_CONSOLE_LEVEL = logging.INFO


PYCONTROLAPI_PYCONTROL_PATH = None
PYCONTROLAPI_DEVICES_PATH = None