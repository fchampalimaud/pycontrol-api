# !/usr/bin/python3
# -*- coding: utf-8 -*-

import logging
import os, json, glob
from send2trash import send2trash
from pycontrolapi.models.setup.setup_base import BaseSetup

logger = logging.getLogger(__name__)


class SetupIO(BaseSetup):
	
	##########################################################################
	####### FUNCTIONS ########################################################
	##########################################################################
	
	def save(self, experiment_path, data):
		if self.name:

			setups_path = os.path.join(experiment_path, 'setups')
			if not os.path.exists(setups_path): os.makedirs(setups_path)
			
			setup_path = os.path.join(setups_path, self.name)
			if not os.path.exists(setup_path): os.makedirs(setup_path)

			self.path = setup_path
			
			data = {
				'name':  self.name,
				'board': self.board.name if self.board else None	
			}

			self.board_task.save(setups_path, data)

			for session in self.sessions: session.save(setup_path, {})

			#remove from the sessions file the unused session files
			sessions_paths = [session.path for session in self.sessions]
			for path in self.__list_all_sessions_in_folder(setup_path):
				if path not in sessions_paths: send2trash(path)

			settings_path = os.path.join(setup_path, 'setup-settings.json')

			with open(settings_path, 'w') as output_file:
				json.dump(data, output_file, sort_keys=True, indent=4, separators=(',', ':'))

			
			return data

		return None


	def load(self, setup_path, data):
		settings_path = os.path.join(setup_path, 'setup-settings.json')
		self.path = setup_path

		with open(settings_path, 'r') as output_file:
			data = json.load(output_file)
			self.name  = data['name']
			self.board = data.get('board', None)

			self.board_task.load(setup_path, data)

		for filepath in self.__list_all_sessions_in_folder(setup_path):
			session = self.create_session()
			session.load(filepath, {})

		self._sessions = sorted(self.sessions, key = lambda x: x.started)

	

		
	def __list_all_sessions_in_folder(self, setup_path):
		search_4_files_path = os.path.join(setup_path, '*.txt')
		return sorted(glob.glob(search_4_files_path))
