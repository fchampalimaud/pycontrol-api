# !/usr/bin/python3
# -*- coding: utf-8 -*-

import logging

from pycontrolapi.exceptions.run_setup import RunSetupError
from pycontrolapi.models.setup.setup_io import SetupIO

from time import sleep

logger = logging.getLogger(__name__)


class ComSetup(SetupIO):
	#### SETUP STATUS ####
	STATUS_READY = 0
	STATUS_BOARD_LOCKED = 1  # The setup is free but the board is busy
	STATUS_INSTALLING_TASK = 2  # The setup is busy installing the board
	STATUS_SYNCING_VARS = 3  # The setup is syncing variables
	STATUS_RUNNING_TASK = 4  # The setup is busy running the task, but it cannot be stopped yet
	STATUS_RUNNING_TASK_HANDLER = 5  # The setup is busy running the task, but it is possible to stopped it
	STATUS_RUNNING_TASK_ABOUT_2_STOP = 6  # The setup is busy running the task, but it is about to stop it

	def __init__(self, experiment):
		super(ComSetup, self).__init__(experiment)
		self.status = ComSetup.STATUS_READY

	##########################################################################
	####### PROPERTIES #######################################################
	##########################################################################

	@property
	def status(self):
		try:
			if self.board is not None and self.board.status == self.board.STATUS_INSTALLING_FRAMEWORK:
				return ComSetup.STATUS_BOARD_LOCKED
			else:
				return self._status
		except Exception as e:
			logger.error(str(e), exc_info=True)

	@status.setter
	def status(self, value):
		self._status = value

		if value == ComSetup.STATUS_READY:
			if hasattr(self, '_run_flag'): del self._run_flag

	##########################################################################
	####### FUNCTIONS ########################################################
	##########################################################################


	def run_task(self):
		if not self.path:
			logger.warning("Run task cannot be executed because project is not saved.")
			raise RunSetupError("Project must be saved before run/upload task")

		if not self.board or not self.task:
			logger.warning("Subject has no task assigned.")
			raise RunSetupError("Please assign a board and task first")

		try:
			self.restore_task_variables_from_session()
			session = self.create_session()

			self._run_flag = self.board.run_task(session, self.board_task)
		except Exception as err:
			logger.error(str(err), exc_info=True)
			raise Exception("Unknown error found while running task. See log for more details.")

	def stop_task(self):
		if hasattr(self, '_run_flag') and self._run_flag:
			self.status = ComSetup.STATUS_RUNNING_TASK_ABOUT_2_STOP
			self.board.write(b'E')
			sleep(0.1)
			self._run_flag.set()

	def install_task(self):
		if not self.path:
			logger.warning("Run task cannot be executed because project is not saved.")
			raise RunSetupError("Project must be saved before run/upload task")

		if not self.board or not self.task:
			logger.warning("Setup has no task assigned.")
			raise RunSetupError("Please assign a board and task first")
		self.board.install_task(self.board_task)

	def restore_task_variables_from_session(self, session=None):
		"""
		:return:
		"""
		logger.debug("Start restoring variables")
		if not session: session = self.last_session

		if not session:
			logger.debug("Subject has no sessions to restore from")
			return

		variables = self.board_task.variables
		for task_variable in variables:
			if task_variable.persistent:
				if len(session.messages_history) == 0: session.load_contents(session.path)

				task_variable.value = session.find_last_var_value(task_variable.name)

		self.board_task.variables = variables
