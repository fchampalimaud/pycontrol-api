# !/usr/bin/python3
# -*- coding: utf-8 -*-

import logging, os, json
from send2trash import send2trash
from pycontrolapi.models.experiment.experiment_base import BaseExperiment

logger = logging.getLogger(__name__)

class ExperimentIO(BaseExperiment):
	
	##########################################################################
	####### FUNCTIONS ########################################################
	##########################################################################

	def save(self, project_path, data):
		"""Build experiment data structure"""
		experiments_path = os.path.join(project_path, 'experiments')
		if not os.path.exists(experiments_path): os.makedirs(experiments_path)

		experiment_path = os.path.join(experiments_path, self.name)
		if not os.path.exists(experiment_path): os.makedirs(experiment_path)

		self.path       = experiment_path

		for setup in self.setups: setup.save(experiment_path, {}) 

		#remove from the setups directory the unused setup files
		setups_paths = [setup.path for setup in self.setups]
		for path in self.__list_all_setups_in_folder(experiment_path):
			if path not in setups_paths: send2trash(path)

		data['name']    = self.name
		data['task']    = self.task.name if self.task else None

		settings_path = os.path.join(experiment_path, 'experiment-settings.json')
		with open(settings_path, 'w') as output_file:
			json.dump(data, output_file, sort_keys=True, indent=4, separators=(',', ':'))


		return data

	def load(self, experiment_path, data): 
		settings_path = os.path.join(experiment_path, 'experiment-settings.json')
		with open(settings_path, 'r') as output_file:
			data = json.load(output_file)

			self.name = data['name']
			self.task = data.get('task', None)

			for path in self.__list_all_setups_in_folder(experiment_path):
				setup = self.create_setup()
				setup.load(path, {})

			self.path = experiment_path
		return data

	def __list_all_setups_in_folder(self, experiment_path):
		search_4_dirs_path = os.path.join(experiment_path, 'setups')
		if not os.path.exists(search_4_dirs_path): return []
		return sorted([os.path.join(search_4_dirs_path, d) for d in os.listdir(search_4_dirs_path) if os.path.isdir(os.path.join(search_4_dirs_path, d))])