# !/usr/bin/python3
# -*- coding: utf-8 -*-

import os, re
import datetime
from pycontrolapi.models.session.session_base import SessionBase
from pycontrolapi.exceptions.invalid_session import InvalidSessionError
from pycontrolapi.models.board.com.msg_factory import parse_board_msg


class SessionIO(SessionBase):

	def save(self, setup_path, data):
		filename = os.path.basename(self.path).replace('.txt', '')
		filepath = os.path.dirname(self.path)

		if filename!=self.name or filepath!=setup_path:

			new_path = os.path.join(setup_path, self.name+'.txt')
			os.rename(self.path, new_path)
			self.path = new_path

	def load(self, session_path, data):
		self.name = os.path.basename(session_path).replace('.txt', '')

		with open(session_path, "r") as f: file_content = f.read()

		try:
			reg 			= re.compile("I\sSubject\sID\s:\s(?P<field>.*)")
			self.setup_name = re.search(reg, file_content).group(1)
		except AttributeError as err:
			raise InvalidSessionError("Invalid session file: {0}".format(session_path), session_path, err)
		except ValueError as err:
			raise InvalidSessionError("Invalid session file: {0}".format(session_path), session_path, err)

		try:
			reg 			= re.compile("I\sBoard\sID\s:\s(?P<field>.*)")
			self.board_name = re.search(reg, file_content).group(1)
		except AttributeError as err:
			raise InvalidSessionError("Invalid session file: {0}".format(session_path), session_path, err)
		except ValueError as err:
			raise InvalidSessionError("Invalid session file: {0}".format(session_path), session_path, err)

		try:
			reg 					= re.compile("I\sBoard\sserial\sport\s:\s(?P<field>.*)")
			self.board_serial_port 	= re.search(reg, file_content).group(1)
		except AttributeError as err:
			raise InvalidSessionError("Invalid session file: {0}".format(session_path), session_path, err)
		except ValueError as err:
			raise InvalidSessionError("Invalid session file: {0}".format(session_path), session_path, err)

		try:
			reg 			= re.compile("I\sTask\sname\s:\s(?P<field>.*)")
			self.task_name 	= re.search(reg, file_content).group(1)
		except AttributeError as err:
			raise InvalidSessionError("Invalid session file: {0}".format(session_path), session_path, err)
		except ValueError as err:
			raise InvalidSessionError("Invalid session file: {0}".format(session_path), session_path, err)

		try:
			reg 			= re.compile("I\sStart\sdate\s:\s(?P<field>.*)")
			start_date_str 	= re.search(reg, file_content).group(1)
			self.started 	= datetime.datetime.strptime(start_date_str, '%Y/%m/%d %H:%M:%S')
		except AttributeError as err:
			raise InvalidSessionError("Invalid session file: {0}".format(session_path), session_path, err)
		except ValueError as err:
			raise InvalidSessionError("Invalid session file: {0}".format(session_path), session_path, err)

		try:
			reg = re.compile("I\sEnd\sdate\s:\s(?P<field>.*)")
			end_date_str = re.search(reg, file_content).group(1)
			self.ended = datetime.datetime.strptime(end_date_str, '%Y/%m/%d %H:%M:%S')
		except AttributeError as err:
			self.ended = None
		except ValueError as err:
			self.ended = None

		self.path = session_path
		
		

		
	def load_contents(self, session_path):
		regex = re.compile("(?P<data_entry>(V|E|S|D|P|I|!)\s.*)")
		board_task = self.setup.create_board_task()

		with open(session_path, "r") as f: file_content = f.read()

		for entry in re.findall(regex, file_content):
			message = parse_board_msg(entry[0], board_task)
			self.messages_history.append(message)
