# !/usr/bin/python3
# -*- coding: utf-8 -*-

import logging, os
from pycontrolapi.models.task.task_base import BaseTask

logger = logging.getLogger(__name__)


class TaskIO(BaseTask):
    
    ##########################################################################
    ####### FUNCTIONS ########################################################
    ##########################################################################

    def save(self, project_path, data):
        #save only if the task file exists
        tasks_path = os.path.join(project_path, 'tasks')
        if not os.path.exists(tasks_path): os.makedirs(tasks_path)
        new_task_path = os.path.join(tasks_path, self.name)+'.py'

        if self.path != new_task_path:
            #if the task file is not in the project file, it makes a copy to the project folder
            code_txt    = self.code if self.path else ''
            self.path   = new_task_path
            self.code   = code_txt
        

    def load(self, task_path, data): 
        
        self.name = os.path.splitext(os.path.basename(task_path))[0]
        self.path = task_path

    