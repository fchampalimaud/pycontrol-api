from pycontrolapi.models.project.project_base import BaseProject


class ProjectUtils(BaseProject):
	
	def find_board(self, name):
		for board in self.boards:
			if board.name == name: return board
		return None

	def find_task(self, name):
		for task in self.tasks:
			if task.name == name: return task
		return None