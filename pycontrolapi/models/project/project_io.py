# !/usr/bin/python
# -*- coding: utf-8 -*-

import os
import logging
import glob
import json
from send2trash import send2trash

from pycontrolapi.models.project.project_utils import ProjectUtils

logger = logging.getLogger(__name__)


class ProjectIO(ProjectUtils):
	##########################################################################
	####### FUNCTIONS ########################################################
	##########################################################################

	def load(self, project_path):
		"""
		Load project from data file
		"""

		settings_path = os.path.join(project_path, 'project-settings.json')

		with open(settings_path, 'r') as input_file:
			data = json.load(input_file)
			self.name = data['name']
			self.path = project_path

			logger.debug("==== LOAD TASKS ====")

			for path in self.__list_all_tasks_in_folder(project_path):
				task = self.create_task()
				task.load(path, {})

			logger.debug("==== LOAD BOARDS ====")

			# load boards
			for path in self.__list_all_boards_in_folder(project_path):
				board = self.create_board()
				board.load(path, {})

			logger.debug("==== LOAD EXPERIMENTS ====")

			# load experiments
			for path in self.__list_all_experiments_in_folder(project_path):
				experiment = self.create_experiment()
				experiment.load(path, {})

			logger.debug("==== LOAD FINNISHED ====")

	##########################################################################

	def save(self, project_path):
		"""Build project data structure"""

		logger.debug("Saving project path: %s", project_path)
		logger.debug("Current project name: %s", self.name)
		logger.debug("Current project path: %s", self.path)

		if not self.path and os.path.exists(project_path):
			raise FileExistsError("Project folder already exists")

		if not os.path.exists(project_path):
			os.mkdir(project_path)
			logger.debug("Created project dir: {}".format(project_path))

		"""
		if self.name != os.path.basename(project_path):
			new_path = os.path.join(os.path.dirname(project_path), self.name)
			os.rename(project_path, new_path)
			project_path = new_path
			logger.debug("Renamed project dir: {}".format(project_path))"""

		########### SAVE THE TASKS ###########
		logger.debug("Saving tasks to {0}".format(project_path))

		for task in self.tasks: task.save(project_path, {})

		# remove from the tasks directory the unused tasks files
		tasks_paths = [task.path for task in self.tasks]
		for path in self.__list_all_tasks_in_folder(project_path):
			if path not in tasks_paths:
				logger.debug("Sending file [{0}] to trash".format(path))
				send2trash(path)

		########### SAVE THE BOARDS ###########
		logger.debug("Saving boards to {0}".format(project_path))

		boards 		 = [board.save(project_path, {}) for board in self.boards]
		
		boards_paths = [board.path for board in self.boards]
		for path in self.__list_all_boards_in_folder(project_path):
			if path not in boards_paths:
				logger.debug("Sending folder [{0}] to trash".format(path))
				send2trash(path)
		

		########### SAVE THE EXPERIMENTS ############
		logger.debug("Saving experiments to {0}".format(project_path))

		experiments = [experiment.save(project_path, {}) for experiment in self.experiments]

		# remove from the experiments directory the unused experiment files
		experiments_paths = [experiment.path for experiment in self.experiments]
		for path in self.__list_all_experiments_in_folder(project_path):
			if path not in experiments_paths:
				logger.debug("Sending directory [{0}] to trash".format(path))
				send2trash(path)

		########### SAVE THE PROJECT ############

		# create root nodes
		data = {
			'name': self.name
		}

		settings_path = os.path.join(project_path, 'project-settings.json')

		with open(settings_path, 'w') as output_file:
			json.dump(data, output_file, sort_keys=True, indent=4, separators=(',', ':'))

		self.path = project_path

		logger.debug("Project saved: %s", settings_path)

	##########################################################################
	####### AUXILIAR FUNCTIONS ###############################################
	##########################################################################


	def __list_all_experiments_in_folder(self, project_path):
		search_4_dirs_path = os.path.join(project_path, 'experiments')
		if not os.path.exists(search_4_dirs_path): return []
		return sorted([os.path.join(search_4_dirs_path, d) for d in os.listdir(search_4_dirs_path) if
		               os.path.isdir(os.path.join(search_4_dirs_path, d))])

	def __list_all_tasks_in_folder(self, project_path):
		path = os.path.join(project_path, 'tasks')
		if not os.path.exists(path): return []
		search_4_files_path = os.path.join(path, '*.py')
		return sorted(glob.glob(search_4_files_path))

	def __list_all_boards_in_folder(self, project_path):
		search_4_dirs_path = os.path.join(project_path, 'boards')
		if not os.path.exists(search_4_dirs_path): return []
		return sorted([os.path.join(search_4_dirs_path, d) for d in os.listdir(search_4_dirs_path) if
		               os.path.isdir(os.path.join(search_4_dirs_path, d))])
