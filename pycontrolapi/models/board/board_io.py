# !/usr/bin/python3
# -*- coding: utf-8 -*-

import os, shutil
import logging, json
from send2trash import send2trash
from pycontrolapi.models.board.board_base import BaseBoard

logger = logging.getLogger(__name__)


class BoardIO(BaseBoard):
	
	##########################################################################
	####### FUNCTIONS ########################################################
	##########################################################################
	
	def save(self, project_path, data):
		if self.name:

			boards_path = os.path.join(project_path, 'boards')
			if not os.path.exists(boards_path): os.makedirs(boards_path)
			
			board_path = os.path.join(boards_path, self.name)

			if self.path and self.path != board_path:

				if os.path.exists(board_path): 
					logger.debug("Sending folder [{0}] to trash".format(path))
					send2trash(path)

				shutil.copytree(self.path, board_path)

			elif not os.path.exists(board_path): 
				os.makedirs(board_path)

			
			data = {
				'name':         self.name,
				'serial_port':  self.serial_port
			}

			settings_path = os.path.join(board_path, 'board-settings.json')

			with open(settings_path, 'w') as output_file:
				json.dump(data, output_file, sort_keys=True, indent=4, separators=(',', ':'))

			self._path  = board_path

			return data


	def load(self, board_path, data):
		settings_path = os.path.join(board_path, 'board-settings.json')
		with open(settings_path, 'r') as output_file:
			data = json.load(output_file)
			self.name           = data['name']
			self.serial_port    = data['serial_port']

			self._path           = board_path
	