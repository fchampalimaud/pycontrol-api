# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrolapi.models.board

"""

import os
import logging


from pycontrolapi.exceptions.no_jobs_running import NoJobsRunning
from pycontrolapi.exceptions.board_job_error import BoardJobError
from serial.serialutil import SerialException
from pycontrolapi.exceptions.board.connection_error import BoardConnectionError
from pycontrolapi.models.board.board_io import BoardIO

logger = logging.getLogger(__name__)


class AsynchronousBoard(BoardIO):
 
    ##########################################################################
    ####### FUNCTIONS ########################################################
    ##########################################################################


    pass