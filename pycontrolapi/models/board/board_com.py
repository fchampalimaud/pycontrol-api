# !/usr/bin/python3
# -*- coding: utf-8 -*-

import logging
import uuid
import datetime
from time import sleep
import os
from pysettings import conf

from pycontrolapi.models.board.board_asynchronous import AsynchronousBoard
from pycontrolapi.models.board.com.msg_factory import parse_board_msg
from pycontrolapi.models.board.exceptions import *
from pycontrolapi.models.board.board_operations import BoardOperations
from pycontrolapi.models.setup.board_task import BoardTask  # used for type checking

logger = logging.getLogger(__name__)


class ComBoard(AsynchronousBoard):

	#### SETUP STATUS ####
	STATUS_READY				= 0
	STATUS_INSTALLING_FRAMEWORK = 1 #The board is busy installing the framework
	STATUS_INSTALLING_TASK		= 2 #The board is busy installing a task
	STATUS_SYNCING_VARS 		= 3	#The board is busy syncing variables
	STATUS_RUNNING_TASK 		= 4	#The board is busy running a task

	INSTALL_FRAMEWORK_TAG = 'Install framework'
	INSTALL_TASK_TAG = 'Upload task'
	SYNC_VARIABLES_TAG = 'Sync variables'
	RUN_TASK_TAG = 'Run task'

	CONSOLE_COMMENT_LEVEL1_TAG = '#1'
	CONSOLE_COMMENT_LEVEL2_TAG = '#2'
	CONSOLE_ERROR_TAG = '!'


	def __init__(self, experiment):
		super(ComBoard,self).__init__(experiment)
		self.status = ComBoard.STATUS_READY

	##########################################################################
	####### PROPERTIES #######################################################
	##########################################################################

	@property
	def status(self): return self._status
	@status.setter
	def status(self, value): self._status = value


	##########################################################################
	####### FUNCTIONS ########################################################
	##########################################################################

	def log_msg_error(self, msg):
		tag_msg = '{0} {1}'.format(self.CONSOLE_ERROR_TAG, msg)  # adds sign to indicate message type
		self.log_msg(tag_msg)

	def log_msg_level1(self, msg):
		tag_msg = '{0} {1}'.format(self.CONSOLE_COMMENT_LEVEL1_TAG, msg)  # adds sign to indicate message type
		self.log_msg(tag_msg)

	def log_msg_level2(self, msg):
		tag_msg = '{0} {1}'.format(self.CONSOLE_COMMENT_LEVEL2_TAG, msg)  # adds sign to indicate message type
		self.log_msg(tag_msg)

	def log_msg(self, msg, board_task=None):
		self.log_messages.append(parse_board_msg(msg, board_task))
		logger.debug(msg)

	def log_session_history(self, session, board_task, msg):
		"""
		Log session history on file and on memory
		We could read the file while writing at the same time
		but this would be a more complex approach
		:param session:
		:param board_task:
		:param msg:
		:return:
		"""
		session.log_msg(msg, board_task)
		self._session_log_file.write(msg)


	def unique_id(self, handler_evt=None):
		if handler_evt is None: handler_evt = self.unique_id_handler_evt
		super(ComBoard, self).unique_id(handler_evt)

	def unique_id_handler_evt(self, e, result):
		self.log_msg('I Board {0} ID: {1}'.format(self.name, result))

	#####################################################################
	##### INSTALL FRAMEWORK #############################################
	#####################################################################

	def install_framework(self, framework_path, devices_path, close=True):
		self.status = ComBoard.STATUS_INSTALLING_FRAMEWORK

		self.log_msg_level1('{0} | Started'.format(self.INSTALL_FRAMEWORK_TAG))

		logger.debug('PYCONTROLAPI_PYCONTROL_PATH: %s', conf.PYCONTROLAPI_PYCONTROL_PATH)
		logger.debug('PYCONTROLAPI_DEVICES_PATH: %s', conf.PYCONTROLAPI_DEVICES_PATH)

		if framework_path is None:
			framework_path = conf.PYCONTROLAPI_PYCONTROL_PATH

		if framework_path is None or not os.path.exists(framework_path):
			self.log_msg_error('{0} | FAILED'.format(self.INSTALL_FRAMEWORK_TAG))
			raise FileNotFoundError("Invalid framework path: {0}".format(framework_path))

		if devices_path is None:
			devices_path = conf.PYCONTROLAPI_DEVICES_PATH


		self.log_msg_level1('{0} | pyControl path: {1}'.format(self.INSTALL_FRAMEWORK_TAG, framework_path))
		self.log_msg_level1('{0} | devices path: {1}'.format(self.INSTALL_FRAMEWORK_TAG, devices_path))

		func_group_id = uuid.uuid4()

		self.log_msg_level1('{0} | Uploading framework, devices and hw definition...'.format(self.INSTALL_FRAMEWORK_TAG))

		self.pycontrol_install_framework(
			framework_path, devices_path, self.hardware_file,
			handler_evt=self.install_framework_handler_evt,
			extra_args=(BoardOperations.INSTALLFRAMEWORK_LOAD_FILES,),
			group=func_group_id
		)

		command = 'import pyControl.framework as fw'
		self.log_msg_level2(command)

		self.exec_command(
			'import pyControl.framework as fw',
			sleep_time=0.5,
			handler_evt=self.install_framework_handler_evt,
			extra_args=(BoardOperations.INSTALLFRAMEWORK_IMPORT_PYCONTROL,),
			group=func_group_id
		)

	def install_framework_handler_evt(self, e, result):
		called_operation = e.extra_args[0]

		if called_operation == BoardOperations.INSTALLFRAMEWORK_LOAD_FILES:
			if isinstance(result, Exception):
				self.log_msg_error(str(result))
				self.status = ComBoard.STATUS_READY
				self.log_msg_error('{0} | FAILED'.format(self.INSTALL_FRAMEWORK_TAG))
				raise result

			self.log_msg_level1('{0} | Testing framework...'.format(self.INSTALL_FRAMEWORK_TAG))

		elif called_operation == BoardOperations.INSTALLFRAMEWORK_IMPORT_PYCONTROL:
			if isinstance(result, Exception):
				self.log_msg_error(str(result))
				self.status = ComBoard.STATUS_READY
				self.log_msg_error('{0} | FAILED'.format(self.INSTALL_FRAMEWORK_TAG))
				raise CannotImportPycontrolError("Invalid framework detected. Please check console for more info.")

			self.log_msg_level1('{0} | Success!'.format(self.INSTALL_FRAMEWORK_TAG))
			self.status = ComBoard.STATUS_READY
		else:
			if isinstance(result, Exception):
				self.log_msg_error(str(result))
				self.status = ComBoard.STATUS_READY
				self.log_msg_error('{0} | FAILED'.format(self.INSTALL_FRAMEWORK_TAG))
				raise result


	#####################################################################
	##### INSTALL TASK ##################################################
	#####################################################################

	def install_task(self, board_task, close=True):
		self.log_msg_level1('{0} | Started'.format(self.INSTALL_TASK_TAG))

		board_task.setup.status = board_task.setup.STATUS_INSTALLING_TASK
		self.status 			= ComBoard.STATUS_INSTALLING_TASK


		func_group_id = uuid.uuid4()

		self.pycontrol_install_task(
			board_task.task.path,
			handler_evt=self.install_task_handler_evt,
			extra_args=(BoardOperations.INSTALLTASK_UPLOAD_TASK_FILE, board_task),
			group=func_group_id
		)

		command = 'from pyControl import *'
		self.log_msg_level2(command)

		self.exec_command(
			command,
			handler_evt=self.install_task_handler_evt,
			extra_args=(BoardOperations.INSTALLTASK_IMPORT_FRAMEWORK, board_task),
			group=func_group_id
		)

		command = 'import {0} as smd; {0} = sm.State_machine(smd);'.format(board_task.task.name)
		self.log_msg_level2(command)
		self.exec_command(
			command,
			handler_evt=self.install_task_handler_evt,
			extra_args=(BoardOperations.INSTALLTASK_IMPORT_TASK, board_task,),
			group=func_group_id
		)

	def install_task_handler_evt(self, e, result):
		called_operation = e.extra_args[0]

		if called_operation == BoardOperations.INSTALLTASK_UPLOAD_TASK_FILE:
			if isinstance(result, FileNotFoundError):
				self.log_msg_error(str(result))
				e.extra_args[1].setup.status 	= e.extra_args[1].setup.STATUS_READY
				self.status 					= ComBoard.STATUS_READY
				raise FileNotFoundError(
					"Task file path not found. Please check your settings.")

			elif isinstance(result, Exception):
				self.log_msg_error(str(result))
				e.extra_args[1].setup.status 	= e.extra_args[1].setup.STATUS_READY
				self.status 					= ComBoard.STATUS_READY
				raise CannotUploadTaskError(
					"Unable to upload task. Please check console for more info.")

			self.log_msg_level1('{0} | Checking framework'.format(self.INSTALL_TASK_TAG))

		elif called_operation == BoardOperations.INSTALLTASK_IMPORT_FRAMEWORK:
			if isinstance(result, Exception):
				self.log_msg_error(str(result))
				e.extra_args[1].setup.status 	= e.extra_args[1].setup.STATUS_READY
				self.status 					= ComBoard.STATUS_READY
				self.log_msg_error('{0} | FAILED'.format(self.INSTALL_TASK_TAG))
				raise CannotImportPycontrolError(
					"Invalid framework detected or not installed. Please reinstall framework.")

			self.log_msg_level1('{0} | Loading state machine'.format(self.INSTALL_TASK_TAG))

		elif called_operation == BoardOperations.INSTALLTASK_IMPORT_TASK:
			if isinstance(result, Exception):
				self.log_msg_error(str(result))
				e.extra_args[1].setup.status 	= e.extra_args[1].setup.STATUS_READY
				self.status 					= ComBoard.STATUS_READY
				self.log_msg_error('{0} | FAILED'.format(self.INSTALL_TASK_TAG))
				raise CannotLoadTaskError(
					"Could not load task. Please check console for more info.")

			self.log_msg_level1('{0} | Success!'.format(self.INSTALL_TASK_TAG))
			e.extra_args[1].setup.status 	= e.extra_args[1].setup.STATUS_READY
			self.status 					= ComBoard.STATUS_READY


	#####################################################################
	##### SYNC VARIABLES ################################################
	#####################################################################

	def sync_variables(self, board_task, close=True, func_group_id=None):
		board_task.setup.status = board_task.setup.STATUS_SYNCING_VARS
		self.status 			= ComBoard.STATUS_SYNCING_VARS

		self.log_msg_level1('{0} | Started'.format(self.SYNC_VARIABLES_TAG))

		if not func_group_id: func_group_id = uuid.uuid4()

		task = board_task.task
		variables = board_task.variables

		self.log_msg_level1('{0} | Checking if task is uploaded'.format(self.SYNC_VARIABLES_TAG))

		command = '{0}.smd'.format(task.name)
		self.log_msg_level2(command)
		self.exec_command(
			command,
			handler_evt=self.sync_variables_handler_evt,
			extra_args=(BoardOperations.SYNCVARIABLES_IMPORT_TASK, board_task,),
			group=func_group_id
		)

		for variable in variables:
			if variable.value:
				command = task.name + '.smd.v.' + variable.name + '=' + variable.value
				self.log_msg_level2(command)
				self.exec_command(
					command,
					handler_evt=self.sync_variables_handler_evt,
					extra_args=(BoardOperations.SYNCVARIABLES_SET_VARIABLE, board_task),
					group=func_group_id
				)
			else:
				command = 'print( repr({0}.smd.v.{1}) )'.format(task.name, variable.name)
				self.log_msg_level2(command)
				self.exec_command(
					command,
					handler_evt=self.sync_variables_handler_evt,
					extra_args=(BoardOperations.SYNCVARIABLES_GET_VARIABLE, board_task, variable),
					group=func_group_id,
					sleep_time=0.1
				)

		command = 'print(0)'
		self.log_msg_level2(command)
		self.exec_command(
			command,
			handler_evt=self.sync_variables_handler_evt,
			extra_args=(BoardOperations.SYNCVARIABLES_PRINT_VARIABLES, board_task, variables),
			group=func_group_id
		)

	def sync_variables_handler_evt(self, e, result):
		called_operation = e.extra_args[0]
		board_task = e.extra_args[1]

		if called_operation == BoardOperations.SYNCVARIABLES_GET_VARIABLE:
			if isinstance(result, Exception):
				self.log_msg(str(result))
				e.extra_args[1].setup.status 	= e.extra_args[1].setup.STATUS_READY
				self.status 					= ComBoard.STATUS_READY
				self.log_msg_error('{0} | FAILED'.format(self.SYNC_VARIABLES_TAG))
				raise GetVariableError("Error when getting variable. Please check console for more info.")
			variable = e.extra_args[2]
			try:
				eval(result)
			except: result = None

			variable.value = result

		elif called_operation == BoardOperations.SYNCVARIABLES_PRINT_VARIABLES:
			if isinstance(result, Exception):
				self.log_msg(str(result))
				e.extra_args[1].setup.status 	= e.extra_args[1].setup.STATUS_READY
				self.status 					= ComBoard.STATUS_READY
				self.log_msg_error('{0} | FAILED'.format(self.SYNC_VARIABLES_TAG))
				raise PrintVariablesError("Error when printing variables. Please check console for more info.")
			variables = e.extra_args[2]
			board_task.variables = variables
			self.log_msg_level1('{0} | Success!'.format(self.SYNC_VARIABLES_TAG))
			e.extra_args[1].setup.status 	= e.extra_args[1].setup.STATUS_READY
			self.status 					= ComBoard.STATUS_READY

		else:
			if isinstance(result, Exception):
				self.log_msg(str(result))
				e.extra_args[1].setup.status 	= e.extra_args[1].setup.STATUS_READY
				self.status 					= ComBoard.STATUS_READY
				self.log_msg_error('{0} | FAILED'.format(self.SYNC_VARIABLES_TAG))
				raise result

	#####################################################################
	##### RUN TASK ######################################################
	#####################################################################

	def run_task(self, session, board_task, close=True):

		self.log_msg_level1('{0} | Started'.format(self.RUN_TASK_TAG))

		if board_task.task.path is None:
			self.log_msg_error('{0} | FAILED'.format(self.RUN_TASK_TAG))
			raise FileNotFoundError('The task file does not exists yet.')

		board_task.setup.status = board_task.setup.STATUS_RUNNING_TASK
		self.status 			= ComBoard.STATUS_RUNNING_TASK

		func_group_id = uuid.uuid4()

		self.log_msg_level1('{0} | Checking if the task is installed...'.format(self.RUN_TASK_TAG))

		command = "{0}".format(board_task.task.name)
		self.log_msg_level2('{0} | $: {1}'.format(self.RUN_TASK_TAG, command))
		self.exec_command(
			command,
			sleep_time=0.5,
			handler_evt=self.run_task_handler_evt,
			extra_args=(BoardOperations.RUNTASK_CHECK_TASK_INSTALLED, session, board_task),
			group=func_group_id
		)

		self.pycontrol_sync_variables(
			board_task.task.name,
			[(v.name, v.value) for v in board_task.variables],
			handler_evt=self.run_task_handler_evt,
			extra_args=(BoardOperations.RUNTASK_SYNC_VARIABLES, session, board_task),
			group=func_group_id
		)

		for variable in board_task.variables:
			command = 'print( repr({0}.smd.v.{1}) )'.format(board_task.task.name, variable.name)
			self.log_msg_level2('{0} | $: {1}'.format(self.RUN_TASK_TAG, command))
			self.exec_command(
				command,
				handler_evt=self.run_task_handler_evt,
				extra_args=(BoardOperations.RUNTASK_PRINT_VARIABLES, session, board_task, variable),
				group=func_group_id
			)

		command = 'import pyControl.framework as fw'
		self.exec_command(
			command,
			sleep_time=0.5,
			handler_evt=self.run_task_handler_evt,
			extra_args=(BoardOperations.RUNTASK_IMPORT_FRAMEWORK, session, board_task),
			group=func_group_id
		)

		command = 'fw.print_events()'
		self.exec_command(
			command,
			eval_output=False,
			sleep_time=0.5,
			handler_evt=self.run_task_handler_evt,
			extra_args=(BoardOperations.RUNTASK_PRINT_EVENTS, session, board_task),
			group=func_group_id
		)

		command = 'fw.print_states()'
		self.exec_command(
			command,
			eval_output=False,
			sleep_time=0.5,
			handler_evt=self.run_task_handler_evt,
			extra_args=(BoardOperations.RUNTASK_PRINT_STATES, session, board_task),
			group=func_group_id
		)

		command = 'fw.verbose = False'
		self.exec_command(
			command,
			sleep_time=0.1,
			handler_evt=self.run_task_handler_evt,
			extra_args=(BoardOperations.RUNTASK_SET_VERBOSE, session, board_task),
			group=func_group_id
		)

		command = 'fw.data_output = True'
		self.exec_command(
			command,
			sleep_time=0.1,
			handler_evt=self.run_task_handler_evt,
			extra_args=(BoardOperations.RUNTASK_SET_DATA_OUTPUT, session, board_task),
			group=func_group_id
		)

		command = 'fw.run()'
		# Return a flag so the main process can stop the loop
		self._run_flag = self.send_command(
			command,
			handler_evt=self.run_task_handler_evt,
			extra_args=(BoardOperations.RUNTASK_RUN_FRAMEWORK, session, board_task),
			group=func_group_id
		)

		return self._run_flag

	def run_task_handler_evt(self, e, result):
		called_operation 	= e.extra_args[0]
		session 			= e.extra_args[1]
		board_task 			= e.extra_args[2]  # type: BoardTask

		if called_operation == BoardOperations.RUNTASK_CHECK_TASK_INSTALLED:
			if isinstance(result, Exception):
				self.log_msg(str(result))
				e.extra_args[2].setup.status 	= e.extra_args[1].setup.STATUS_READY
				self.status 					= ComBoard.STATUS_READY
				self.log_msg_error('{0} | FAILED'.format(self.RUN_TASK_TAG))
				raise TaskNotFoundError( "The task {0} was not detected.".format(board_task.task.name) )

			# NEXT TASK
			self.log_msg_level1('{0} | Writing variables on board...'.format(self.RUN_TASK_TAG))
			self.log_msg_level2('{0} | $: from pyControl import *'.format(self.RUN_TASK_TAG))
			self.log_msg_level2('{0} | $: import {1} as smd; {1} = sm.State_machine(smd);'.format(self.RUN_TASK_TAG, board_task.task.name))
			for var in board_task.variables:
				if var.name is not None:
					self.log_msg_level2(
						'{0} | $: {1}.smd.v.{2} = {3}'.format(self.RUN_TASK_TAG, board_task.task.name, var.name, var.value))

		elif called_operation == BoardOperations.RUNTASK_SYNC_VARIABLES:
			if isinstance(result, Exception):
				self.log_msg(str(result))
				e.extra_args[2].setup.status 	= e.extra_args[1].setup.STATUS_READY
				self.status 					= ComBoard.STATUS_READY
				self.log_msg_error('{0} | FAILED'.format(self.RUN_TASK_TAG))
				raise result

			self._session_log_file = open(session.path, 'w+', newline='\n', buffering=1)

			self.log_session_history(session, board_task, ("I Experiment name : {0}\n".format(session.setup.experiment.name)))
			self.log_session_history(session, board_task, ("I Subject ID : {0}\n".format(session.setup_name)))
			self.log_session_history(session, board_task, ("I Start date : {0}\n".format(datetime.datetime.now().strftime('%Y/%m/%d %H:%M:%S'))))
			self.log_session_history(session, board_task, ("I Board ID : {0}\n".format(session.board_name)))
			self.log_session_history(session, board_task, ("I Board serial port : {0}\n".format(session.board_serial_port)))
			self.log_session_history(session, board_task, ("I Task name : {0}\n".format(session.task_name)))
			self.log_session_history(session, board_task, ("\n"))

			self.log_session_history(session, board_task, ("## Read task variables:\n"))

#			self.log_msg_level1('{0} | Exporting task variables: {1}'.format(self.RUN_TASK_TAG, [(variable.name, variable.value) for variable in board_task.variables]))
			# NEXT TASK
			self.log_msg_level1('{0} | Reading variables from board...'.format(self.RUN_TASK_TAG))


		elif called_operation == BoardOperations.RUNTASK_PRINT_VARIABLES:
			if isinstance(result, Exception):
				self.log_msg(str(result))
				e.extra_args[2].setup.status 	= e.extra_args[1].setup.STATUS_READY
				self.status 					= ComBoard.STATUS_READY
				self.log_msg_error('{0} | FAILED'.format(self.RUN_TASK_TAG))
				raise PrintVariablesError("Error when printing variables. Please check console for more info.")

			variable = e.extra_args[3]

			try:
				eval(result)
			except: result = None

			self.log_session_history(session, board_task, ("V {{'{0}' : {1}}}".format(variable.name, result) + '\n'))

			self.log_msg_level1('{0} | Board variable exported to file: {1}'.format(self.RUN_TASK_TAG, "V {{'{0}' : {1}}}".format(variable.name, result)))
			self.log_msg_level2('{0} | host var {1} : {2}'.format(self.RUN_TASK_TAG, variable.name, variable.value))
			self.log_msg_level2('{0} | board var {1}: {2}'.format(self.RUN_TASK_TAG, variable.name, str(result)))


		elif called_operation == BoardOperations.RUNTASK_IMPORT_FRAMEWORK:
			self.log_msg_level1('{0} | Checking framework...'.format(self.RUN_TASK_TAG))
			self.log_msg_level2('{0} | $: import pyControl.framework as fw'.format(self.RUN_TASK_TAG))

			if isinstance(result, Exception):
				self.log_msg(str(result))
				e.extra_args[2].setup.status 	= e.extra_args[1].setup.STATUS_READY
				self.status 					= ComBoard.STATUS_READY
				self.log_msg_error('{0} | FAILED'.format(self.RUN_TASK_TAG))
				raise CannotImportPycontrolError(
					"Invalid framework detected or not installed. Please reinstall framework.")

			# NEXT TASK
			self.log_msg_level1('{0} | Reading board events...'.format(self.RUN_TASK_TAG))
			self.log_msg_level2('{0} | $: fw.print_events()'.format(self.RUN_TASK_TAG))

		elif called_operation == BoardOperations.RUNTASK_PRINT_EVENTS:
			if isinstance(result, Exception):
				self.log_msg(str(result))
				e.extra_args[2].setup.status 	= e.extra_args[1].setup.STATUS_READY
				self.status 					= ComBoard.STATUS_READY
				self.log_msg_error('{0} | FAILED'.format(self.RUN_TASK_TAG))
				raise PrintEventsError("Error when printing events. Please check console for more info.")

			self.log_session_history(session, board_task, ("\n## Read events:\n"))
			self.log_session_history(session, board_task, (result + '\n\n'))

			self.log_msg_level2('{0} | host events: {1}'.format(self.RUN_TASK_TAG, [event for event in board_task.events]))
			self.log_msg_level2('{0} | board events: {1}'.format(self.RUN_TASK_TAG, str(result)))

			self.log_msg_level1('{0} | Board events saved on file: {1}'.format(self.RUN_TASK_TAG, str(result)))

			# NEXT TASK
			self.log_msg_level1('{0} | Reading board states...'.format(self.RUN_TASK_TAG))
			self.log_msg_level2('{0} | $: fw.print_states()'.format(self.RUN_TASK_TAG))

		elif called_operation == BoardOperations.RUNTASK_PRINT_STATES:
			if isinstance(result, Exception):
				self.log_msg(str(result))
				e.extra_args[2].setup.status 	= e.extra_args[1].setup.STATUS_READY
				self.status 					= ComBoard.STATUS_READY
				self.log_msg_error('{0} | FAILED'.format(self.RUN_TASK_TAG))
				raise PrintStatesError("Error when printing states. Please check console for more info.")

			self.log_session_history(session, board_task, ("## Read states:\n"))
			self.log_session_history(session, board_task, result + '\n')

			self.log_session_history(session, board_task, ("\n\n"))
			self.log_session_history(session, board_task, ("## OUTPUT:\n"))

			e.extra_args[2].setup.status = e.extra_args[1].setup.STATUS_RUNNING_TASK_HANDLER


			self.log_msg_level1('{0} | Board states saved on file: {1}'.format(self.RUN_TASK_TAG, str(result)))
			self.log_msg_level2('{0} | host states: {1}'.format(self.RUN_TASK_TAG, [state for state in board_task.states]))
			self.log_msg_level2('{0} | board states: {1}'.format(self.RUN_TASK_TAG, str(result)))

			# NEXT TASK
			self.log_msg_level1('{0} | Running task now...'.format(self.RUN_TASK_TAG))
			self.log_msg_level2('{0} | $: fw.verbose = False'.format(self.RUN_TASK_TAG))
			self.log_msg_level2('{0} | $: fw.data_output = True'.format(self.RUN_TASK_TAG))
			self.log_msg_level2('{0} | $: fw.run()'.format(self.RUN_TASK_TAG))

		elif called_operation == BoardOperations.RUNTASK_RUN_FRAMEWORK:
			if isinstance(result, Exception):
				self.log_msg(str(result))
				self.write(b'E') #An error occurs, send the comand to terminate the state machine
				#sleep(1)
				self._run_flag.set()
				del self._run_flag
				e.extra_args[2].setup.status 	= e.extra_args[1].setup.STATUS_READY
				self.status 					= ComBoard.STATUS_READY
				self.log_msg_error('{0} | FAILED'.format(self.RUN_TASK_TAG))
				raise RunTaskError("Error while running task. Please check console for more info.")

			elif result is not None:
				result = result[:-1]  # .replace('\n\r', '')
				self.log_msg(result, board_task)
				self.log_session_history(session, board_task, (result + '\n'))

			if e.last_call:
				#self.write(b'E')
				#self.write(b'\x04')

				self.log_msg_level1('{0} | Reading variables from board...'.format(self.RUN_TASK_TAG))

				self.log_session_history(session, board_task, ("\n\n## Read task variables:\n"))

				for variable in board_task.variables:
					command = 'print( repr({0}.smd.v.{1}) )'.format(board_task.task.name, variable.name)
					self.log_msg_level2('{0} | $: {1}'.format(self.RUN_TASK_TAG, command))
					self.exec_command(
						command,
						handler_evt=self.run_task_handler_evt,
						extra_args=(BoardOperations.RUNTASK_PRINT_NEW_VARIABLES, session, board_task, variable),
						sleep_time=0.1
					)

				command = 'print(0)'
				self.log_msg_level2('{0} | $: {1}'.format(self.RUN_TASK_TAG, command))
				self.exec_command(
					command,
					handler_evt=self.run_task_handler_evt,
					extra_args=(BoardOperations.RUNTASK_STOP_FRAMEWORK, session, board_task)
				)

				#sleep(5)
				self._run_flag.clear()
				del self._run_flag

		elif called_operation == BoardOperations.RUNTASK_PRINT_NEW_VARIABLES:
			if isinstance(result, Exception):
				self.log_msg(str(result))
				e.extra_args[2].setup.status 	= e.extra_args[1].setup.STATUS_READY
				self.status 					= ComBoard.STATUS_READY
				self.log_msg_error('{0} | FAILED'.format(self.RUN_TASK_TAG))
				raise PrintStatesError("Error when printing states. Please check console for more info.")

			variable = e.extra_args[3]

			try:
				eval(result)
			except: result = None

			self.log_msg_level1('{0} | Board variable exported to file: {1}'.format(self.RUN_TASK_TAG, "V {{'{0}' : {1}}}".format(variable.name, result)))
			self.log_msg_level2('{0} | host var {1} : {2} (maybe not updated yet)'.format(self.RUN_TASK_TAG, variable.name, variable.value))
			self.log_msg_level2('{0} | board var {1}: {2}'.format(self.RUN_TASK_TAG, variable.name, str(result)))

			if hasattr(self, '_session_log_file'):
				self.log_session_history(session, board_task, "V {{'{0}' : {1}}}".format(variable.name, result) + '\n')

		elif called_operation == BoardOperations.RUNTASK_STOP_FRAMEWORK:
			self.log_msg_level1('{0} | Success!'.format(self.RUN_TASK_TAG))
			if hasattr(self, '_session_log_file'):
				self.log_session_history(session, board_task,
				                         "\n\nI End date : {0}\n".format(datetime.datetime.now().strftime('%Y/%m/%d %H:%M:%S')))
				self._session_log_file.close()
				del self._session_log_file

			logger.debug('------ stoping setup [{0}] -------'.format( str(e.extra_args[2].setup) ))
			e.extra_args[2].setup.status 	= e.extra_args[1].setup.STATUS_READY
			self.status 					= ComBoard.STATUS_READY
