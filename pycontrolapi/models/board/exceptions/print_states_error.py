# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pycontrolapi.models.board.exceptions.board_error import BoardError


class PrintStatesError(BoardError):
	pass
