# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrolapi.models.board

"""

import os
import logging
from pyboard_communication.async_pyboard import AsyncPyboard

logger = logging.getLogger(__name__)


class BaseBoard(AsyncPyboard):
	"""
	Board represents the hardware that controls the running session for a specific subject
	"""

	def __init__(self, project):
		AsyncPyboard.__init__(self)
		
		self.name           = 'Untitled board {0}'.format( len(project.boards) )
		self.serial_port    = None
		self.project        = project

		self._path          = None
		self.log_messages   = []

		self.project        += self

	##########################################################################
	####### PROPERTIES #######################################################
	##########################################################################

	@property
	def name(self): return self._name
	@name.setter
	def name(self, value): self._name = value

	@property
	def serial_port(self): return self._serial_port
	@serial_port.setter
	def serial_port(self, serial_port): self._serial_port = serial_port

	@property
	def project(self): return self._project
	@project.setter
	def project(self, value): self._project = value

	@property
	def hardware_file(self):
		if self.path:
			hw = os.path.join(self.path, 'hardware.py')
			if os.path.isfile(hw): return hw
		
		return None
  
	@property
	def path(self): return self._path

	

	##########################################################################
	####### FUNCTIONS ########################################################
	##########################################################################

	def remove(self): pass

	def __unicode__(self):  return self.name

	def __str__(self):      return self.__unicode__()

	