# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pycontrolapi.models.board.com.board_message import BoardMessage


class EventFired(BoardMessage):
	"""Message from board that represents an event fired"""

	MESSAGE_TYPE_ALIAS = 'event_fired'

	def __init__(self, message, board_timestamp, eventcode, events_names):
		self.board_timestamp = board_timestamp
		self.event_id = eventcode
		self.event_name = events_names[self.event_id]

		super(EventFired, self).__init__(content=message,
		                                 format_string='{0} {1} {2}'.format(self.board_timestamp, self.event_id,
		                                                                    self.event_name))

		@property
		def event_name(self):
			""" Event name """
			return self._event_name

		@event_name.setter
		def event_name(self, value):
			"""
			event name setter
			:param value: event name
			:type value: string
			"""
			self._event_name = value

		@property
		def event_id(self):
			""" Event id """
			return self._event_id

		@event_id.setter
		def event_id(self, value):
			"""
			event id setter
			:param value: event id
			:type value: string
			"""
			self._event_id = value
