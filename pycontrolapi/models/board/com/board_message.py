# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pycontrolapi.models.board.com.history_message import HistoryMessage


class BoardMessage(HistoryMessage):
	""" Represents a message output from the board """
	pass
