# !/usr/bin/python3
# -*- coding: utf-8 -*-

import logging
from pycontrolapi.models.board.com.session_metadata import SessionMetadata
from pycontrolapi.models.board.com.print_statement import PrintStatement
from pycontrolapi.models.board.com.error_message import ErrorMessage
from pycontrolapi.models.board.com.event_fired import EventFired
from pycontrolapi.models.board.com.state_entry import StateEntry
from pycontrolapi.models.board.com.events_list import EventsList
from pycontrolapi.models.board.com.states_list import StatesList
from pycontrolapi.models.board.com.debug_message import DebugMessage
from pycontrolapi.models.board.com.board_id import BoardId
from pycontrolapi.models.board.com.variables_values import VariablesValues

logger = logging.getLogger(__name__)


def parse_board_msg(data="", board_task=None):
	"""
	Parse board message and return appropriate event
	:param board_task: the board and task association
	:type board_task: pycontrolapi.models.board_task
	:param data: data to be parsed
	:type data: string
	:returns: instance of HistoryMessage
	"""
	if not data:
		logger.warning("Data is empty")
		return ErrorMessage(data)

	try:
		data = data.strip()
		message_code = data.split(' ')[0]  # get message code (character variable number string before the first space)


		if message_code == 'P':  # User generated print statement
			parsed_message = PrintStatement(data)

		elif message_code == '!':
			parsed_message = ErrorMessage(data)

		elif message_code == 'E' and board_task:  # Event dictionary
			parsed_message = EventsList(data)
			board_task.events = parsed_message.events  # update task events

		elif message_code == 'S' and board_task:  # State dictionary
			parsed_message = StatesList(data)
			board_task.states = parsed_message.states  # update task states

		elif message_code == 'V' and board_task:  # Variable value
			parsed_message = VariablesValues(data)
			board_task.set_variable(parsed_message.variable_name, parsed_message.variable_value)

		elif message_code == 'I':  # Information line for things like experiment name , task name etc.
			# parsed_message = BoardId(data)
			return SessionMetadata(data)

		elif message_code == 'D':  # Event occurrence/state entry data in format: timestamp ID
			# WE ARE ASSUMING THAT BOARD RUNS ONLY ONE TASK AT A TIME

			message_segments = data.strip().split(' ')  # D 24565 2
			message_code, board_timestamp, eventcode = message_segments
			board_timestamp, eventcode = int(board_timestamp), int(eventcode)

			if board_task and board_task.is_state_change(int(eventcode)):
				parsed_message = StateEntry(data, board_timestamp, int(eventcode), board_task.states)
			else:
				parsed_message = EventFired(data, board_timestamp, int(eventcode), board_task.events)

		elif message_code.startswith('#'):  # application debug message (not printed to session file)
			parsed_message = DebugMessage(data)  # default case

		else:  # no match at all
			logger.warning("No matching format for board data message: {0}".format(data), exc_info=True)
			parsed_message = DebugMessage(data)  # default case
	except Exception as err:
		# The msg is considered a comment for the cases where the events formats are not correct
		logger.warning("Unexpected format for board data message: {0}".format(data), exc_info=True)
		parsed_message = ErrorMessage(data)  # default case

	logger.debug('Parsed message: {0} | Message type: {1}'.format(parsed_message, str(type(parsed_message))))

	return parsed_message
