# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrolapi.models.board.com.variables_value

"""

from pycontrolapi.models.board.com.board_message import BoardMessage
import logging

__author__ = "Carlos Mão de Ferro and Ricardo Ribeiro"
__copyright__ = ""
__cred_its__ = "Carlos Mão de Ferro and Ricardo Ribeiro"
__license__ = "MIT"
__version__ = "0.0"
__maintainer__ = ["Ricardo Ribeiro", "Carlos Mão de Ferro"]
__email__ = ["ricardojvr at gmail.com", "cajomferro at gmail.com"]
__status__ = "Development"
__updated__ = "2015-11-17"

logger = logging.getLogger(__name__)


class VariablesValues(BoardMessage):
	MESSAGE_TYPE_ALIAS = 'set_variable'

	def __init__(self, message):
		variable_dict = eval(message[2:])  # V {'period' : 2}
		self.variable_name = next(iter(variable_dict.keys()))
		self.variable_value = next(iter(variable_dict.values()))

		super(VariablesValues, self).__init__(content=message,
		                                      format_string='{0} | {1}'.format(self.variable_name, self.variable_value))

	@property
	def variable_name(self):
		""" States list """
		return self._variable_name

	@variable_name.setter
	def variable_name(self, value):
		self._variable_name = value

	@property
	def variable_value(self):
		return self._variable_value

	@variable_value.setter
	def variable_value(self, value):
		self._variable_value = value
