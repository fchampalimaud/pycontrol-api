# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pycontrolapi.models.board.com.board_message import BoardMessage


class SessionMetadata(BoardMessage):
	""" Information line for things like experiment name , task name, board id, etc. """
	MESSAGE_TYPE_ALIAS = 'metadata'

	def __init__(self, message):
		message_segments = message.strip().split(' ')  # I Experiment name : exp1
		colon_pos = message_segments.index(':')
		self.metadata_key = " ".join(message_segments[1:colon_pos])
		self.metadata_value = " ".join(message_segments[colon_pos + 1:])

		super(SessionMetadata, self).__init__(content=message,
		                                      format_string='{key} | {value}'.format(key=self.metadata_key,
		                                                                             value=self.metadata_value))

	@property
	def metadata_key(self):
		return self._metadata_key

	@metadata_key.setter
	def metadata_key(self, value):
		self._metadata_key = value

	@property
	def metadata_value(self):
		return self._metadata_value

	@metadata_value.setter
	def metadata_value(self, value):
		self._metadata_value = value
