# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrolapi.models.board.com.comment

"""

import pycontrolapi
from pycontrolapi.models.board.com.board_message import BoardMessage

__version__ = pycontrolapi.__version__
__author__ = pycontrolapi.__author__
__credits__ = pycontrolapi.__credits__
__license__ = pycontrolapi.__license__
__maintainer__ = pycontrolapi.__maintainer__
__email__ = pycontrolapi.__email__
__status__ = pycontrolapi.__status__
__updated__ = "2016-07-19"


class ErrorMessage(BoardMessage):
    """ Message that represents an error """
    MESSAGE_TYPE_ALIAS = 'unexpected_format'
