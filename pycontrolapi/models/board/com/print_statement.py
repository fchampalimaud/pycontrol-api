# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pycontrolapi.models.board.com.board_message import BoardMessage


class PrintStatement(BoardMessage):
	"""
	User generated print statement.
	If users want to use characters in their strings to indicate things
	like comments that is up to them but there is no specific support for this.
	"""

	MESSAGE_TYPE_ALIAS = 'print_statement'

	def __init__(self, message):
		message_segments = message.strip().split(' ')  # P 27473 Rewards obtained: 1

		self.board_timestamp = int(message_segments[1])
		self.print_statement = " ".join(message_segments[2:])

		super(PrintStatement, self).__init__(content=message,
		                                     format_string='{0} {1}'.format(self.board_timestamp, self.print_statement))

	@property
	def print_statement(self):
		return self._print_statement

	@print_statement.setter
	def print_statement(self, value):
		self._print_statement = value
