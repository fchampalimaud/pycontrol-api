# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrolapi.models.board.com.board_id

"""

from pycontrolapi.models.board.com.board_message import BoardMessage

__author__ = "Carlos Mão de Ferro and Ricardo Ribeiro"
__copyright__ = ""
__cred_its__ = "Carlos Mão de Ferro and Ricardo Ribeiro"
__license__ = "MIT"
__version__ = "0.0"
__maintainer__ = ["Ricardo Ribeiro", "Carlos Mão de Ferro"]
__email__ = ["ricardojvr at gmail.com", "cajomferro at gmail.com"]
__status__ = "Development"
__updated__ = "2015-11-23"


class BoardId(BoardMessage):

    def __init__(self, content):
        super(BoardId, self).__init__(content)
        self.board_id = eval(content[4:]).decode('utf8')  # ? I b'<\x00>\x00\x14Q332574'

    @property
    def board_id(self):
        """ board_id """
        return self._board_id

    @board_id.setter
    def board_id(self, value):
        """
        board_id setter
        :param value: board_id (parsed from original content)
        :type value: string
        """
        self._board_id = value
