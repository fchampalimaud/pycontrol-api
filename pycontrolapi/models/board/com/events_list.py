# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pycontrolapi.models.board.com.board_message import BoardMessage


class EventsList(BoardMessage):
	""" Message representing the list of current events loaded on the board """

	MESSAGE_TYPE_ALIAS = 'events'

	def __init__(self, message):
		super(EventsList, self).__init__(message)
		# pylint: disable=eval-used

		events = eval(message[2:])  # E {'timer_evt': 3}
		self.events = dict((v, k) for k, v in events.items())  # switch keys and values

		super(EventsList, self).__init__(content=message,
		                                 format_string='{0}'.format(self.events))

	@property
	def events(self):
		""" Events list """
		return self._events

	@events.setter
	def events(self, value):
		"""
		events setter
		:param value: events list (parsed from original content)
		:type value: string
		"""
		self._events = value
