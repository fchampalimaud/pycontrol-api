# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pycontrolapi.models.board.com.board_message import BoardMessage


class StateEntry(BoardMessage):
	""" Message from board that represents a state entry"""

	MESSAGE_TYPE_ALIAS = 'state_change'

	def __init__(self, message, board_timestamp, state_id, states_names):
		self.board_timestamp = board_timestamp
		self.state_id = state_id
		self.state_name = states_names[self.state_id]

		super(StateEntry, self).__init__(content=message,
		                                 format_string='{0} | {1} | {2}'.format(self.board_timestamp, self.state_id,
		                                                                        self.state_name))

	@property
	def state_name(self):
		""" State name """
		return self._state_name

	@state_name.setter
	def state_name(self, value):
		"""
		state name setter
		:param value: state name
		:type value: string
		"""
		self._state_name = value

	@property
	def state_id(self):
		""" State id """
		return self._state_id

	@state_id.setter
	def state_id(self, value):
		"""
		state id setter
		:param value: state id
		:type value: string
		"""
		self._state_id = value
