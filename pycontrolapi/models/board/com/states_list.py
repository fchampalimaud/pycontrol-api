# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pycontrolapi.models.board.com.board_message import BoardMessage


class StatesList(BoardMessage):
	""" Message representing the list of current states loaded on the board """

	MESSAGE_TYPE_ALIAS = 'states'

	def __init__(self, message):
		super(StatesList, self).__init__(message)
		# pylint: disable=eval-used
		states = eval(message[2:])  # S {'LED_on': 1, 'LED_off': 2}
		self.states = dict((v, k) for k, v in states.items())  # switch keys and values

		super(StatesList, self).__init__(content=message,
		                                 format_string='{0}'.format(self.states))

	@property
	def states(self):
		""" States list """
		return self._states

	@states.setter
	def states(self, value):
		"""
		states setter
		:param value: states list (parsed from original content)
		:type value: string
		"""
		self._states = value
