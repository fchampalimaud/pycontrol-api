from pyControl.utility import *

# States and events.

states = ['LED_on', 'LED_off']

events = ['timer_evt']

initial_state = 'LED_on'

# Variables.

v.LED_n = 1  # Number of LED to use.
v.time_dur = 12

# Define behaviour.

def LED_on(event):
    if event == 'entry':
        set_timer('timer_evt', v.time_dur * second)
        pyb.LED(v.LED_n).on()
    elif event == 'exit':
        pyb.LED(v.LED_n).off()
    elif event == 'timer_evt':
        goto('LED_off')


def LED_off(event):
    if event == 'entry':
        set_timer('timer_evt', v.time_dur * second)
    elif event == 'timer_evt':
        goto('LED_on')


def run_end():  # Turn off hardware at end of run.
    next_value = 0
    while next_value == 0 or next_value > 5:
        next_value = int(random() * 10)
    v.time_dur = next_value
    pyb.LED(v.LED_n).off()
