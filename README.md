# PyControl-API
**PyControl-API** is a library that defines the behavioral experiments model.

## Packaging (ONLY FOR DEVELOPERS)

    python setup.py sdist --formats=zip


## Installation
To install this project run:

    python setup.py install