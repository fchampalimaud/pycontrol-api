#!/usr/bin/python3
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
import re

version = ''
with open('pycontrolapi/__init__.py', 'r') as fd:
	version = re.search(r'^__version__\s*=\s*[\'"]([^\'"]*)[\'"]', fd.read(), re.MULTILINE).group(1)

if not version:
	raise RuntimeError('Cannot find version information')

requirements = [
	'Send2Trash >= 1.3.0',
	'pyserial >= 3.1.1',
	'pysettings >= 1.1',
	'logging-bootstrap>=0.1',
]

setup(
	name='pycontrol-api',
	version=version,
	description="""pyControlGUI is a behavioral experiments control system written in Python 3.5 """,
	author='Carlos Mão de Ferro',
	author_email='cajomferro@gmail.com, ricardojvr@gmail.com',
	license='Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>',
	url='https://bitbucket.org/fchampalimaud/pycontrol-api',

	include_package_data=True,
	packages=find_packages(exclude=['contrib', 'docs', 'tests', 'examples', 'deploy', 'reports']),

	# install_requires=requirements,
	entry_points={
		'console_scripts':[
			'pycontrol-create-project=pycontrolapi.scripts:create_project',
		]
	}
)
